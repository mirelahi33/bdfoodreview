<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantArea extends Model
{
    public function restaurant(){

        return $this->belongsTo('App\restaurant','restaurant_id','id');
    }
    public function area(){

        return $this->belongsTo('App\Area','area_id','id');
    }

}