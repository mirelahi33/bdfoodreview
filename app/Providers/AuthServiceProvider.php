<?php

namespace App\Providers;


use App\reviews;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        Schema::defaultStringLength(119);
        $this->registerPolicies($gate);

        $gate->define('isRestaurantModerator',function ($user){
            return $user->user_type =='restaurantmoderator';

        });
        $gate->define('isUser',function ($user){
            return $user->user_type =='user';

        });
        $gate->define('isSuperAdmin',function ($user){
            return $user->user_type =='superadmin';

        });



        //
    }
}
