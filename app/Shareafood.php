<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Shareafood extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'shareafood_caption'
            ]
        ];
    }
    public function restaurant(){

        return $this->belongsTo('App\restaurant');
    }

    public function item(){

        return $this->belongsTo('App\Item');
    }

  /*  public function offer(){

        return $this->belongsTo('App\Offer');
    }*/
    public function offer(){

        return $this->belongsTo('App\Offer');
    }
    public function area(){

        return $this->belongsTo('App\Area');
    }

    public function shareafoodcomments()
    {
        return $this->hasMany('App\Shareafoodcomment');
    }
}
