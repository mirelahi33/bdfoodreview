<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shareafoodcomment extends Model
{
    public function shareafood()
    {
        return $this->belongsTo('App\Shareafood');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
