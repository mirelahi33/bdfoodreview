<?php

namespace App;
use Cviebrock\EloquentSluggable\Sluggable;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'offer_name'
            ]
        ];
    }
  /*  public function shareafood(){
        return $this->hasMany('App\Shareafood');

    }*/
    public function shareafood(){
        return $this->hasMany('App\Shareafood');

    }
    public function restaurant(){
        return $this->belongsTo('App\restaurant');

    }
    public function area(){
        return $this->belongsTo('App\Area');

    }
    public function item(){
        return $this->belongsTo('App\Item');

    }

    public function offercomments()
    {
        return $this->hasMany('App\Offercomment');
    }
}
