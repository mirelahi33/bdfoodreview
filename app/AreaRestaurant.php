<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AreaRestaurant extends Model
{
    public function restaurants(){

        return $this->belongsTo('App\restaurant','restaurant_id','id');
    }
    public function areas(){

        return $this->belongsTo('App\Area','area_id','id');
    }

}