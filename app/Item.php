<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Item extends Model
{

    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'item_name'
            ]
        ];
    }

    protected $table = 'items';
    public function restaurant(){

        return $this->belongsTo('App\restaurant');
    }
    public function review(){

        return $this->hasMany('App\reviews');
    }
    public function shareafood(){

        return $this->hasMany('App\Shareafood');
    }
    public function offer(){

        return $this->hasMany('App\Offer');
    }



  /*  public function item_restaurant(){
        return $this->hasMany('App\ItemRestaurant','item_id','id');

    }*/

}
