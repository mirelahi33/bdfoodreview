<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemRestaurant extends Model
{
    public function restaurant(){

        return $this->belongsTo('App\restaurant','restaurant_id','id');
    }
    public function item(){

        return $this->belongsTo('App\Item','item_id','id');
    }

}