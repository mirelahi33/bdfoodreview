<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferRestaurant extends Model
{
    public function restaurant(){

        return $this->belongsTo('App\restaurant','restaurant_id','id');
    }
    public function Offer(){

        return $this->belongsTo('App\Offer','offer_id','id');
    }

}