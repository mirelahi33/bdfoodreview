<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public function restaurant(){

        return $this->hasMany('App\restaurant');
    }
     public function restaurants(){

         return $this->belongsToMany('App\restaurant');
     }

    public function shareafood(){

        return $this->hasMany('App\Shareafood');
    }

    public function offer(){

        return $this->hasMany('App\Offer');
    }
}
