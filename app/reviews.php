<?php

namespace App;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class reviews extends Model
{  use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'comment'
            ]
        ];
    }

    protected $table = 'reviews';
    public function restaurant(){

        return $this->belongsTo('App\restaurant','restaurant_id','id');
    }

    public function area(){

        return $this->belongsTo('App\Area');
    }
    public function item(){

        return $this->belongsTo('App\Item');
    }
    public function reviewcomments()
    {
        return $this->hasMany('App\Reviewcomment');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
