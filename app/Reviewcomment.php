<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewcomment extends Model
{
    public function reviews()
    {
        return $this->belongsTo('App\reviews');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
