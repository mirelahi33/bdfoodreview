<?php

namespace App\Http\Controllers;

use App\Offer;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pages2/home');
    }

    public function show_review()
    {

        return view("pages/review");
    }
    public function write_a_review()
    {

        return view("pages/write_a_review");
    }
    public function offers()
    {

        return view("pages/offers");
    }
    public function share_a_food()
    {

        return view("pages/share_a_food");
    }
    public function foodgraphy()
    {

        return view("pages/foodgraphy");
    }

    public function create_pages()
    {

        return view("pages2/create_pages");
    }

}
