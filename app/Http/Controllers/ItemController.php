<?php

namespace App\Http\Controllers;

use App\Area;
use App\Item;
use Illuminate\Http\Request;
use App\restaurant;
use App\User;
use App\ItemRestaurant;
use Intervention\Image\ImageManagerStatic as Image;
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // dd($aa);
      //  $items = Item::all();
      /*  if(\Auth::user()->restaurant==null)
        {$x = restaurant::all();
            return view('items.create')->withx($x);}*/

        if(\Auth::user()->restaurant==null)
        {    $x = Area::all();

            $y = \Auth::user();

            return view("restaurant.create")->withx($x)
                ->withy($y);}


        else
        {
            $aa= \Auth::user()->restaurant->id;
           // dd($aa);
            $items= Item::all()->where('restaurant_id','==',$aa);
          //  dd($items->item_name);
            /*$items=\Auth::user()->item;
            $aa= \Auth::user()->id;*/
         //   dd($items);
            return view('items.index')->withitems($items);}



        }
    public function allitem()
    {
        // dd($aa);
          $items = Item::all();

        return view('items.allitem')->withItems($items);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  $x = restaurant::all();
        return view('items.create')->withx($x);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(

            'price'   => 'integer',

        ));

      //  dd($request->all());
        $items = new item;
        $items->item_name =$request->item_name;
       /* $items->restaurant_id =$request->restaurant_name;*/
        $items->restaurant_id =\Auth::user()->restaurant->id;
        $items->price =$request->price;

        if ($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);

            $items->item_image = $filename;
        }

     //  dd($items);
        $items->save();
        session()->flash('success','It has been saved successfully!');

        return redirect()->route('item.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::find($id);
        return view('items.show')->withitem($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
       // dd($request->all());

    }

    public function gg(Request $request)
    {
      //  dd($request->ff);
       // $item = Item::find($id);
//dd($item);
        $item = Item::find($request->ff);

       $a=$item->id;
       $item->owners_recommendation = 1;
        $item->update();
  // dd($b);
       // $item->owners_recommendation = 1;
      //  $kk=Item::all()->except($request->ff);
        //$kk->owners_recommendation = 0;

        $b= $item->id;
        $aa= \Auth::user()->id;
      // dd($aa);
      $cc= Item::where('restaurant_id','=',$aa)->where('id','!=',$b)->update(['owners_recommendation' => 0]);
      // dd($cc);


        //Item::where('id','!=',$b)->update(['owners_recommendation' => 0]);
     /*   $item->update();*/


       // $kk->save();

        return redirect()->back()->with('message', 'recommendation updated');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }

    public function getSingle($slug) {
        //  dd($slug);
        //  return $slug;
        // fetch from the DB based on slug
        $item = Item::where('slug', '=', $slug)->first();
        //dd($item);

        // return the view and pass in the post object
        return view('items.single')->withItem($item);
    }

    public function get_items(Request $request){
//        dd($request);
        $items = Item::all()->pluck('item_name','id')->toArray();
        return response()->json([json_encode([$items])]);
    }
}
