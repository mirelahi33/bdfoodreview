<?php

namespace App\Http\Controllers;

use App\RestaurantArea;
use App\Shareafood;
use Illuminate\Http\Request;
use App\restaurant;
use App\Area;
use App\Item;
use App\Offer;
use App\User;
use App\AreaRestaurant;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Gate;


class ShareafoodController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show','shareafood_search','getSingle']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {/*$shareafoods = AreaRestaurant::all();*/
        $shareafood = Shareafood::all();

/*dd($shareafood->offers());*/

        return view('shareafood.index')->withshareafood($shareafood);
    }
    public function shareafood_search()
    {/*$shareafoods = AreaRestaurant::all();*/
        $shareafood = Shareafood::all();

        /*dd($shareafood->offers());*/

        return view('shareafood.search')->withshareafood($shareafood);
    }

   /* public function share_a_food()
    {

        return view("pages/share_a_food");
    }*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::check('isSuperAdmin')||Gate::check('isRestaurantModerator')){
            return view("pages2.error");
        }
        $a=Offer::all();
        $x = restaurant::all();
        $y=Area::all();
        $z=Item::all();
        $yy = \Auth::user();

        return view("shareafood.create")->witha($a)
            ->withx($x)
            ->withy($y)
            ->withz($z)
            ->withz($z)
            ->withyy($yy);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shareafood = new Shareafood();
        $shareafood->shareafood_caption =$request->details;
        $shareafood->estimate_time =$request->estimate_time;
        $shareafood->estimate_date =$request->estimate_date;
        $shareafood->no_of_person =$request->no_of_person;
        $shareafood->offer_id =$request->offer_name;
        $shareafood->restaurant_id =$request->restaurant_name;
        $shareafood->item_id =$request->item_name;
        $shareafood->area_id =$request->area_name;
        $shareafood->user_id =$request->name;

        if ($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);

            $shareafood->shareafood_image = $filename;
        }

        $shareafood->save();
        session()->flash('success','It has been saved successfully');

        return redirect()->route('shareafood.index'/*,$restaurant->id*/);
    }

    public function getSingle($slug) {
        //  dd($slug);
        //  return $slug;
        // fetch from the DB based on slug
        $shareafood = Shareafood::where('slug', '=', $slug)->first();
        // dd($reviews);

        // return the view and pass in the post object
        return view('shareafood.single')->withShareafood($shareafood);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function show(Shareafood $shareafood)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function edit(Shareafood $shareafood)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shareafood $shareafood)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shareafood $shareafood)
    {
        //
    }
}
