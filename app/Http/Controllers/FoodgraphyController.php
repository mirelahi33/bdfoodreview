<?php

namespace App\Http\Controllers;

use App\Foodgraphy;
use Illuminate\Http\Request;

class FoodgraphyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Foodgraphy  $foodgraphy
     * @return \Illuminate\Http\Response
     */
    public function show(Foodgraphy $foodgraphy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Foodgraphy  $foodgraphy
     * @return \Illuminate\Http\Response
     */
    public function edit(Foodgraphy $foodgraphy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Foodgraphy  $foodgraphy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Foodgraphy $foodgraphy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Foodgraphy  $foodgraphy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Foodgraphy $foodgraphy)
    {
        //
    }
}
