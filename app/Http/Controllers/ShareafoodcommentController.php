<?php

namespace App\Http\Controllers;

use App\Shareafood;
use App\Shareafoodcomment;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class ShareafoodcommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shareafood = Shareafood::find($request->shareafood_id);

        $shareafoodcomment = new Shareafoodcomment();
        $shareafoodcomment->name = $request->name;
        /*   $reviewcomment->email = $request->email;*/
        $shareafoodcomment->comment = $request->comment;
        $shareafoodcomment->approved = true;
        $shareafoodcomment->user_id=\Auth::user()->id;
        $shareafoodcomment->shareafood()->associate($shareafood);
        //  dd($reviewcomment);



        $shareafoodcomment->save();

        // Session::flash('success', 'Comment was added');

        return redirect()->route('shareafood.single', [$shareafood->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shareafoodcomment  $shareafoodcomment
     * @return \Illuminate\Http\Response
     */
    public function show(Shareafoodcomment $shareafoodcomment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shareafoodcomment  $shareafoodcomment
     * @return \Illuminate\Http\Response
     */
    public function edit(Shareafoodcomment $shareafoodcomment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shareafoodcomment  $shareafoodcomment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shareafoodcomment $shareafoodcomment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shareafoodcomment  $shareafoodcomment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shareafoodcomment $shareafoodcomment)
    {
        //
    }
}
