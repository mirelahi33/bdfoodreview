<?php

namespace App\Http\Controllers;

use App\Area;
use App\AreaRestaurant;
use App\Item;
use App\ItemRestaurant;
use App\OfferRestaurant;
use App\restaurant;
use App\RestaurantArea;
use Illuminate\Http\Request;
use App\User;
use App\Offer;
use Intervention\Image\ImageManagerStatic as Image;


class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
       // $this->middleware('auth:admin');
    }

    public function index()
    {


        if(\Auth::user()->restaurant==null)
        {    $x = Area::all();

            $y = \Auth::user();



            return view("restaurant.create")->withx($x)
                ->withy($y);}
        else
        {
            $aa= \Auth::user()->restaurant->id;
          //   dd($aa);
            $restaurants= restaurant::all()->where('id','==',$aa);
            /*$items=\Auth::user()->item;
            $aa= \Auth::user()->id;*/
           //    dd($restaurants);
            return view('restaurant.index')->withrestaurants($restaurants);}




  /*      //
        $restaurants = restaurant::all();

        //dd($restaurants);
        return view('restaurant.index')->withrestaurants($restaurants);*/
    }

    public function allrestaurant()
    {
        // dd($aa);
        $restaurants = restaurant::all();

        return view('restaurant.allrestaurants')->withRestaurants($restaurants);



    }


    public function get_restaurants(Request $request){
        //dd($request);
        return response()->json([json_encode([restaurant::findorFail($request->restaurant_id)])]);
    }
 public function get_restaurant_info(Request $request){

     $restaurant_area = AreaRestaurant::with(['restaurants','areas'])->where('restaurant_id',$request->restaurant_id)->get()->pluck('areas.area_name','area_id')->toArray();

     //$restaurant_area = RestaurantArea::with(['restaurant','Area'])->where('restaurant_id',$request->restaurant_id)->get()->pluck('Area.area_name','area_id')->toArray();
       $restaurant_item =  Item::where('restaurant_id',$request->restaurant_id)->get()->pluck('item_name','id')->toArray();
     $restaurant_offer = Offer::where('restaurant_id',$request->restaurant_id)->get()->pluck('offer_name','id')->toArray();

     return response()->json([json_encode([$restaurant_area,$restaurant_item,$restaurant_offer])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $x = Area::all();

        $y = \Auth::user();



        return view("restaurant.create")->withx($x)
                                        ->withy($y);

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


            $restaurant = new restaurant;
        $restaurant->restaurant_name =$request->restaurant_name;
        $restaurant->address =$request->address;
        $restaurant->user_id = $request->name;
        $restaurant->area_id = $request->area;

        if ($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);

            $restaurant->restaurant_logo = $filename;
        }
        $restaurant->save();

        $restaurant->areas()->sync($request->area, false);
        session()->flash('success','the restaurant  was successfully save!');

        return redirect()->route('restaurant.index'/*,$restaurant->id*/);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
     $restaurant = restaurant::find($id);
     return view('restaurant.show')->withrestaurant($restaurant);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $restaurant = restaurant::find($id);
        return view('restaurant.edit')->withrestaurant($restaurant);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $restaurant = restaurant::find($id);
        $restaurant->restaurant_name= $request->input('restaurant_name');
        $restaurant->address= $request->input('address');
        $restaurant->save();
        return redirect()->route('restaurant.show',$restaurant->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $restaurant = restaurant::find($id);
        $restaurant->delete();
        return redirect()->route('restaurant.index');


    }
    public function getSingle($slug) {
        //  dd($slug);
        //  return $slug;
        // fetch from the DB based on slug
        $restaurant = restaurant::where('slug', '=', $slug)->first();
        // dd($reviews);

        // return the view and pass in the post object
        return view('restaurant.single')->withRestaurant($restaurant);
    }



    public function get_all_restaurant(Request $request){
//        dd($request);
        $restaurants = restaurant::all()->pluck('restaurant_name','id','address','slug')->toArray();
        return response()->json([json_encode([$restaurants])]);
    }

    public function all_restaurant(Request $request){
//        dd($request);
        $restaurants = restaurant::where('id',$request->id)->get()->toArray();
        return response()->json([json_encode([$restaurants])]);
    }
}
