<?php

namespace App\Http\Controllers;

use App\Area;
use Illuminate\Http\Request;


class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area = Area::all();
        return view('area.index')->witharea($area);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    $x = Area::all();


        return view("area.create")->withx($x);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, array(
            'area_name'         => 'required|max:255',

        ));


        $area = new Area();
        $area->area_name =$request->area_name;

        $area->save();
        session()->flash('success','It has been saved successfully!');
        return redirect()->route('area.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $area = Area::find($id);
        return view('area.show')->witharea($area);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        //
    }

    public function get_areas(Request $request){
//        dd($request);
        $areas = Area::all()->pluck('area_name','id')->toArray();
        return response()->json([json_encode([$areas])]);
    }

    public function all_areas(Request $request){
//        dd($request);
        $areas = Area::with(['Offer.Item','restaurant.Item'])->where('id',$request->id)->get()->toArray();
        return response()->json([json_encode([$areas])]);
    }
}
