<?php

namespace App\Http\Controllers;

use App\reviews;
use Illuminate\Http\Request;
use App\restaurant;
use App\Area;
use App\Item;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Gate;


class ReviewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show','reviews_search','getSingle','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = reviews::all();
     //   dd($reviews);
        return view('reviews.index')->withreviews($reviews);
    }

    public function reviews_search()
    {
        $reviews = reviews::all();
        //  dd($reviews);
        return view('reviews.search')->withreviews($reviews);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::check('isSuperAdmin')||Gate::check('isRestaurantModerator')){
       return view("pages2.error");
        }

        $x = restaurant::all();
      $y=Area::all();
        $z=Item::all();

        return view("reviews.create")->withx($x)
                                      ->withy($y)
                                      ->withz($z);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
/*            'reviewer_name'         => 'required|max:255',*/
            'rating'         =>   'required|integer|between:1,10,'

        ));
$a=Auth::user()->id;
 $aa = reviews::where('user_id','=',$a)->get()->toArray();

        foreach ($aa as $aaa) {
           //dd( $request->item_name);
           // dd($aaa['item_id']);
            if($request->item_name == $aaa['item_id']){

                return view('pages2.error2');
            }
        }
 //dd('good');
        $reviews = new reviews();
        $reviews->reviewer_name =\Auth::user()->name;
        $reviews->restaurant_id =$request->restaurant_name;
        $reviews->item_id =$request->item_name;
        $reviews->area_id =$request->area_name;
        $reviews->user_id =\Auth::user()->id;


        $reviews->rating =$request->rating;
        $reviews->comment =$request->comment;


        if ($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);

            $reviews->review_image = $filename;
        }

        $reviews->save();
        session()->flash('success','It has been saved successfully!');

        $review = reviews::find($reviews->id);
        session()->flash('success','It has been saved successfully!');

//        return redirect()->route('reviews.index'/*,$reviews->id*/);
        return redirect()->route('reviews.single', [$review->slug]);

    }

    public function getSingle($slug) {
      //  dd($slug);
      //  return $slug;
        // fetch from the DB based on slug
        $reviews = reviews::where('slug', '=', $slug)->first();
       // dd($reviews);

        // return the view and pass in the post object
        return view('reviews.single')->withReviews($reviews);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function show(reviews $reviews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function edit(reviews $reviews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, reviews $reviews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function destroy(reviews $reviews)
    {
        //
    }
}
