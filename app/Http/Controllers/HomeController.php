<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use App\Offer;
use App\reviews;
use App\restaurant;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Access\Gate;
use App\User;

class HomeController extends Controller
{
    public function index()
    {
        $most_reviewed=reviews::select('item_id', DB::raw('count(*) as total'))
            ->with(['Item.restaurant'])
            ->groupBy('item_id','restaurant_id')->orderBy('total','desc')
            ->get()
            ->first()->toArray();

    //  dd($most_reviewed['item']['slug']);
        $latest_offer = Offer::with(['Item.restaurant'])->orderBy("id",'desc')->take(3)->get()->toArray();

       // dd($latest_offer[0]['item']['restaurant']['restaurant_name']);
        // $latest_offer = Offer::with(['Restaurant','Item'])->orderBy('created_at','desc')->paginate(1)->toArray();
        $restaurants = restaurant::orderBy('id','desc')->take(3)->with(['area.restaurant'])->get()->toArray();
       // dd($restaurants);
       // $search_fields = restaurant::all()->sortByDesc("id")->take(3)->pluck('restaurant_name','restaurant_logo');
        $recomendation= Item::all()->where('owners_recommendation','=',1)->random(3)/*sortByDesc("id")->take(3)*/;
    //    $recomendation_single=Item::all()->where('owners_recommendation','=',1)->sortByDesc("id")/*->toArray()*/;
       // dd($recomendation_single);
       // dd($recomendation);
     //  $restaurants = restaurant::all()->pluck('restaurant_name','id');
//        $top_reviewer=reviews::groupBy('reviewer_name')
//            ->orderBy('count', 'desc')
//            ->get(['id', DB::raw('count(reviewer_name) as count')])
//            ->first()->toArray();
        $top_reviewer = reviews::select('user_id', DB::raw('count(*) as total'))
            ->with(['User'])
            ->orderBy('total','desc')
            ->groupBy('user_id')
            ->get()
            ->first()->toArray();
      // dd($top_reviewer);


//dd($reviews);
     //  dd($restaurants);
        // dd($latest_offer['data']);
//        foreach($restaurants as $a){
//            dd($a);
//        }
//dd($latest_offer);
        return view("pages2/home",compact('latest_offer','restaurants','reviews','recomendation','most_reviewed','top_reviewer','recomendation_single'));
    }
    public function contact(){

        return view('pages2.contact');
    }
    public function owners(){

        $recomendation_single=Item::all()->where('owners_recommendation','=',1)->sortByDesc("id")/*->toArray()*/;

  //  dd($recomendation_single);
        return view('items.recommendindex',compact('recomendation_single'));
    }

    public function about(){

        return view('pages2.about');
    }

}
