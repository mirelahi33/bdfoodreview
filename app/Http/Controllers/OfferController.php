<?php

namespace App\Http\Controllers;

use App\Offer;
use Illuminate\Http\Request;
use App\restaurant;
use App\Item;
use App\Area;
use Intervention\Image\ImageManagerStatic as Image;


class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show','offer_search','getSingle']]);
    }


    public function index()
    {
        $offers = Offer::all();


        return view('offer.index')->withoffers($offers);
    }
    public function offer_search()
    {
        $offers = Offer::all();


        return view('offer.search')->withoffers($offers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $x = Area::all();
        $y = restaurant::all();
      //  $z = Item::all();
        $aa= \Auth::user()->restaurant->id;
        // dd($aa);
        $items= Item::all()->where('restaurant_id','==',$aa);
/*$ff=\Auth::user()->restaurant->area->area_name;
dd($ff);*/

        return view("offer.create")->withx($x)
                                   ->withy($y)
                                  /* ->withz($z)*/
                                    ->withitems($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(

            'offer_price'   => 'integer',

        ));

        $offer = new Offer();
        $offer->offer_name =$request->offer_name;
        $offer->offer_details =$request->offer_details;
        $offer->offer_duration_from =$request->offer_duration_from;
        $offer->offer_duration_to =$request->offer_duration_to;
        $offer->price =$request->offer_price;
        $offer->restaurant_id =\Auth::user()->restaurant->id;
        $offer->item_id =$request->item_name;
        $offer->area_id =\Auth::user()->restaurant->area->id;

        if ($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);

            $offer->offer_image = $filename;
        }

        $offer->save();
        session()->flash('success','It has been saved successfully');

        return redirect()->route('offer.index');
    }


    public function getSingle($slug) {
        //  dd($slug);
        //  return $slug;
        // fetch from the DB based on slug
        $offer = Offer::where('slug', '=', $slug)->first();
        // dd($reviews);

        // return the view and pass in the post object
        return view('offer.single')->withOffer($offer);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }
}
