<?php

namespace App\Http\Controllers;

use App\Reviewcomment;
use App\reviews;
use Illuminate\Http\Request;

class ReviewcommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request/*,$review_id*/)
    {
/*        $idd=\Auth::user()->id;
dd($idd);*/
        $review = reviews::find($request->review_id);

        $reviewcomment = new Reviewcomment();
      $reviewcomment->name = $request->name;
        /*   $reviewcomment->email = $request->email;*/
        $reviewcomment->comment = $request->comment;
        $reviewcomment->approved = true;
        $reviewcomment->user_id=\Auth::user()->id;
        $reviewcomment->reviews()->associate($review);
      //  dd($reviewcomment);

        $reviewcomment->save();

       // Session::flash('success', 'Comment was added');

        return redirect()->route('reviews.single', [$review->slug]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Reviewcomment  $reviewcomment
     * @return \Illuminate\Http\Response
     */
    public function show(Reviewcomment $reviewcomment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reviewcomment  $reviewcomment
     * @return \Illuminate\Http\Response
     */
    public function edit(Reviewcomment $reviewcomment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reviewcomment  $reviewcomment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reviewcomment $reviewcomment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reviewcomment  $reviewcomment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reviewcomment $reviewcomment)
    {
        //
    }
}
