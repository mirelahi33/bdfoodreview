<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Offercomment;
use Illuminate\Http\Request;

class OffercommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $offer = Offer::find($request->offer_id);

        $offercomment = new Offercomment();
        $offercomment->name = $request->name;
        /*   $reviewcomment->email = $request->email;*/
        $offercomment->comment = $request->comment;
        $offercomment->approved = true;
        $offercomment->user_id=\Auth::user()->id;
        $offercomment->offer()->associate($offer);
        //  dd($reviewcomment);

        $offercomment->save();

        // Session::flash('success', 'Comment was added');

        return redirect()->route('offer.single', [$offer->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offercomment  $offercomment
     * @return \Illuminate\Http\Response
     */
    public function show(Offercomment $offercomment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offercomment  $offercomment
     * @return \Illuminate\Http\Response
     */
    public function edit(Offercomment $offercomment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offercomment  $offercomment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offercomment $offercomment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offercomment  $offercomment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offercomment $offercomment)
    {
        //
    }
}
