<?php

namespace App;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class restaurant extends Model
{

    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'restaurant_name'
            ]
        ];
    }
    protected $table = 'restaurants';

    public function reviews(){
        return $this->hasMany('App\reviews');

    }

    public function restaurant_area(){
        return $this->hasMany('App\RestaurantArea','restaurant_id','id');

    }

    public function item_restaurant(){
        return $this->hasMany('App\ItemRestaurant','restaurant_id','id');

    }

    public function offer_restaurant(){
        return $this->hasMany('App\OfferRestaurant','restaurant_id','id');

    }

   public function area()
   {
       return $this->belongsTo('App\Area');
   }
    public function areas(){
        return $this->belongsToMany('App\Area','area_restaurants');

}
    public function item(){
        return $this->hasMany('App\Item');

    }
    public function user(){

        return $this->belongsTo('App\User');
    }

    public function shareafood(){
        return $this->hasMany('App\Shareafood');

    }
    public function offer(){
        return $this->hasMany('App\Offer');

    }
}
