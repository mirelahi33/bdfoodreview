<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareafoodcommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('shareafoodcomments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('comment');
            $table->boolean('approved');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('shareafood_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('shareafoodcomments', function ($table){
            $table->foreign('shareafood_id')->references('id')->on('shareafoods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shareafoodcomments');
    }
}
