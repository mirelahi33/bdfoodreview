<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffercommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('offercomments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('comment');
            $table->boolean('approved');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('offer_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('offercomments', function ($table){
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offercomments');
    }
}
