<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareafoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shareafoods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shareafood_caption')->nullable();
            $table->double('no_of_person')->nullable();
            $table->date('estimate_date')->nullable();
            $table->string('estimate_time')->nullable();
            $table->integer('restaurant_id')->unsigned()->nullable();
            $table->integer('item_id')->unsigned()->nullable();
            $table->integer('area_id')->unsigned()->nullable();
           // $table->integer('offer_id')->unsigned()->nullable();
        /*    $table->unsignedInteger('offer_id');
            $table->foreign('offer_id')->references('id')->on('offers');*/

            $table->unsignedInteger('user_id');

                $table->foreign('user_id')->references('id')->on('users');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shareafoods');
    }
}
