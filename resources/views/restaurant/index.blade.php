@extends('main')

@section('content')

    <div class="row">
       <div class="col-md-10">

           <h1>Restaurant</h1>

       </div>
{{--@if(Auth::user()->restaurant==null)

        <div class="col-md-2">

            <a href="{{ route('restaurant.create') }}" class="btn btn-primary btn-block">create restaurant</a>

        </div>
        @endif--}}

<div class="row">
    <div class="col-md-12">
        <table class="table" id="restaurants">

            <thead>
            <th>restaurant name</th>
            <th>restaurant logo</th>
            <th>area </th>
            <th>address</th>
            <th>created at</th>
            <th>Actions</th>

            </thead>
<tbody>
  @foreach($restaurants as $restaurant)
<tr>
    <td>{{$restaurant->restaurant_name}}</td>
    @if ($restaurant->restaurant_logo==null)  <td></td>
    @else  <td><img src="{{asset('/images/'.$restaurant->restaurant_logo)}}" alt="restaurant_logo" height="200" width="200"></td>@endif




{{--{{ dd($restaurant->area)  }}--}}
        <td> {{$restaurant->area->area_name}}</td>


     {{--   <td>{{$restaurant->user}}</td>--}}

    <td>{{str_limit($restaurant->address, 20)}}</td>
    <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
    <td>  <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
                {{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}
                <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿</td>
</tr>
@endforeach

</tbody>
        </table>


    </div>

</div>

    </div>﻿

    @endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#restaurants').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection