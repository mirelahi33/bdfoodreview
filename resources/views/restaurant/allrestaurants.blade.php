@extends('main')

@section('content')

    <div class="row">
       <div class="col-md-10">

           <h1>Restaurant</h1>

       </div>


<div class="row">
    <div class="col-md-12">
        <table class="table" id="restaurants">

            <thead>

            <th>restaurant name</th>
            <th>restaurant logo</th>
{{--
            <th>user id</th>
--}}
            <th>area </th>
            <th>address</th>
            <th>created at</th>


            </thead>
<tbody>
  @foreach($restaurants as $restaurant)
<tr>


    <td>{{$restaurant->restaurant_name}}</td>
    @if ($restaurant->restaurant_logo==null)  <td></td>
    @else  <td><img src="{{asset('/images/'.$restaurant->restaurant_logo)}}" alt="restaurant_logo" height="200" width="200"></td>@endif



{{--
     <td>{{$restaurant->user->id}}</td>
--}}
        <td> {{$restaurant->area->area_name}}</td>
    <td>{{str_limit($restaurant->address, 20)}}</td>
    <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>

</tr>
@endforeach

</tbody>
        </table>


    </div>

</div>

    </div>﻿

    @endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#restaurants').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection