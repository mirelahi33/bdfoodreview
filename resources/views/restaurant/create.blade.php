@extends('main')

@section('content')

    <div class="row">
        <!--content-->
        <section class="content center full-width">
            <div class="modal container">
                <form  method="POST" enctype="multipart/form-data" action="{{ route('restaurant.store') }}" {{--name="contactform" id="contactform"--}}>
                    <h1 class="center">Add a restaurant</h1>
                    <div id="message" class="alert alert-danger"></div>
                    <div class="f-row">
                        <label name="restaurant_name">Restaurant name:</label>
                        <input type="text" id="restaurant_name" name="restaurant_name" class="form-control" required="">
                    </div>

                    <div class="f-row">
                        <input type="hidden" id="name" name="name" class="form-control" value="{{$y->id}}" >
                    </div>

                    <div class="f-row">
                        <label name="area_name">Restaurant area:</label>
                        <select name="area" id="area" class="form-control" required="">
                            <option value=""> -- Select One --</option>
                            @foreach ($x as $area_name)
                                <option value="{{ $area_name->id }}">{{ $area_name->area_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="f-row">
                        <label name="address">Address:</label>
                        <textarea id="address" name="address" rows="10" class="form-control" required=""></textarea>
                    </div>
                    <div class="f-row">
                        <label class="col-md-3 control-label" >image:</label>
                        <div class="col-md-8">
                            <input  type="file" id="featured_img" placeholder="featured_img" name="featured_img" class=""/>
                            <span class="required" id='spnFileError'></span>
                        </div>
                    </div>



                    <div class="f-row bwrap">
                        <input type="submit" value="Add a Restaurant" />
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
            </div>
        </section>
        <!--//content-->
    </div>

    @endsection

{{--

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Create New Restaurant</h1>
        <hr>
        <form method="POST" action="{{ route('restaurant.store') }}">
            <div class="form-group">
                <label name="restaurant_name">Restaurant name:</label>
                <input id="restaurant_name" name="restaurant_name" class="form-control">
            </div>


            <div class="form-group">
                <label name="name">user id:</label>
                <input id="name" name="name" class="form-control" value="{{$y->id}}" readonly>
            </div>






            <div class="form-group">
                <label name="area_name">Restaurant area:</label>
                <select name="area" id="area" class="form-control">
                    <option value=""> -- Select One --</option>
                    @foreach ($x as $area_name)
                        <option value="{{ $area_name->id }}">{{ $area_name->area_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label name="address">Address:</label>
                <textarea id="address" name="address" rows="10" class="form-control"></textarea>
            </div>


            <input type="submit" value="Create Post" class="btn btn-success btn-lg btn-block">
            <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
</div>﻿--}}
