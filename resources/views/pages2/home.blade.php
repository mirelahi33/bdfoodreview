@extends('main')
@section('content')

    <main class="main" role="main">
        <!--intro-->
        <div class="intro">
            <figure class="bg"><img src="{{asset('assets/images/intro.jpg')}}" alt=""/></figure>

            <!--wrap-->
            <div class="wrap clearfix">
                <!--row-->
                <div class="row">
                    <article class="three-fourth text">
                        <h1>Welcome to BdFoodReviews!</h1>
                        <p> This is a <strong>food review website</strong>, where foodies can find the food reviews of their desired restarurants.Wanna know what you will gain by joining us?</p>
                        <p> Register or Login to join us today!</p>

                        @guest
                        <a href="/register" class="button white more medium">Join our community <i
                                    class="fa fa-chevron-right"></i></a>
                        <p>Already a member? Click <a href="/login">here</a> to login.</p>
                        @endguest
                    </article>

                    <!--search recipes widget-->
                    <div class="one-fourth">
                        <div class="widget container">
                            <div class="textwrap">
                                <h3>Search for Restaurants</h3>
                                <p>All you need to do is enter a Restaurant Name.</p>
                                <p>You can also select a specific Restaurant from the dropdown.</p>
                                <p>Enjoy!</p>
                            </div>
                            <form method="post">
                                {{-- <div class="f-row">
                                     <input type="text"  placeholder="Enter your search term" />
                                 </div>--}}
                                <div class="f-row">
                                    <select class="search_item"  style="width:100%">
                                        <option >Select</option>
                                        <option value="1">Restaurant</option>
{{--                                     <option value="2">Areas</option>

                                        <option value="3">Items</option>
--}}
                                    </select>


                                </div>
                                <div class="f-row">


                                    <select class="search_result_list" style="width:100%">
                                    </select>
                                    <div id="searched_data">

                                    </div>
                                </div>
                               {{-- <div class="f-row bwrap">
                                    <input type="submit" value="Search!"/>
                                </div>--}}
                            </form>
                        </div>
                    </div>
                    <!--//search recipes widget-->
                </div>
                <!--//row-->
            </div>
            <!--//wrap-->
        </div>
        <!--//intro-->

        <!--wrap-->
        <div class="wrap clearfix">
            <!--row-->
            <div class="row">


                <!--content-->
                <section class="content three-fourth">
                    <!--cwrap-->
                    <div class="cwrap">
                        <!--entries-->
                        <div class="entries row">
                            <!--featured recipe-->
                            <div class="featured two-third">
                                <header class="s-title">
                                    <h2 class="ribbon">Most Reviewed</h2>
                                </header>
                                <article class="entry">
                                    <figure>
                                        <img src="{{asset('images/'.$most_reviewed['item']['item_image'])}}" width="570px" height="430px" alt="Most Reviewed image" />

                                        <figcaption><a href="item/tag/{{$most_reviewed['item']['slug']}}"><i class="icon icon-themeenergy_eye2"></i>
                                                <span>view most reviewed</span></a></figcaption>
                                    </figure>
                                    <div class="container">
                                        <h2>{{$most_reviewed['item']['item_name']}}</h2>
                                        <h2>{{$most_reviewed['item']['restaurant']['restaurant_name']}}</h2>

                                        {{--
                                                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                                        --}}
                                       {{-- <div class="actions">
                                            <div>
                                                <a href=""class="button">See the full review</a>
                                                <div class="more"><a href="recipes2.html">See the most reviewed foods here!</a></div>
                                            </div>
                                        </div>--}}
                                    </div>
                                </article>
                            </div>
                            <!--//featured recipe-->

                            <!--featured member-->
                            <div class="featured one-third">
                                <header class="s-title">
                                    <h2 class="ribbon star">Top Reviewer</h2>
                                </header>
                                <article class="entry">
                                    <figure>
                                        <img src="{{asset('images/'.$top_reviewer['user']['featured_img'])}}" alt="" />

                                       {{-- <figcaption><a href="my_profile.html"><i class="icon icon-themeenergy_eye2"></i>
                                                <span>View review</span></a></figcaption>--}}
                                    </figure>
                                    <div class="container">
                                        <h1>Top Reviewer</h1>


                                                                                <h2>{{$top_reviewer['user']['name']}}</h2>

                                        {{-- <div class="actions">
                                             <div>
                                                 <a href="#" class="button">Check out her recipes</a>
                                                 <div class="more"><a href="#">See past featured members</a></div>
                                             </div>
                                         </div>--}}
                                    </div>
                                </article>
                            </div>
                            <!--//featured member-->
                        </div>
                        <!--//entries-->
                    </div>
                    <!--//cwrap-->
                    <div class="cwrap">
                        <header class="s-title">
                            <h2 class="ribbon bright">Latest Offers!!!</h2>
                        </header>
                        <!--entries-->
                        <div class="entries row">
                        @foreach($latest_offer as $latest)
                            <!--item-->
                                <div class="entry one-third">
                                    <figure>
                                        {{--<img src="{{asset('images/'.$latest['offer_image'])}}" width="270px" height="200px" alt=""/>--}}
                                        @if ($latest['offer_image'] == null) <img src="{{asset('images/no.jpg')}}"height="200" width="270" alt="restaurant">
                                        @else  <img src="{{asset('images/'.$latest['offer_image'])}}"height="200" width="270" alt="review image">
                                        @endif

                                        <figcaption><a href="offer/tag/{{$latest['slug']}}"><i
                                                        class="icon icon-themeenergy_eye2"></i>
                                                <span>View post</span></a></figcaption>
                                    </figure>
                                    <div class="container">
                                        <h2>{{$latest['offer_name']}}</h2>
                                        <div class="actions">
                                            <div>
                                                <div class="date">Created at<pre> </pre><i class="fa fa-calendar"></i>{{$latest['created_at']}}</div>
                                               {{-- <div class="comments"><i class="fa fa-comment"></i>
                                                    <a href="blog_single.html#comments">27</a></div>--}}
                                            </div>
                                        </div>
                                        <div class="excerpt">
                                            <h2>{{$latest['item']['restaurant']['restaurant_name']}}</h2>
                                        </div>
                                    </div>
                                </div>
                                <!--item-->
                            @endforeach


                            <div class="quicklinks">
                                <a href="/offer" class="button">More posts</a>
{{--
                                <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
--}}
                            </div>
                        </div>
                        <!--//entries-->
                    </div>
                    <div class="cwrap">
                        <header class="s-title">
                            <h2 class="ribbon bright">Owners' Choice</h2>
                        </header>
                        <!--entries-->
                        <div class="entries row">

                        @foreach($recomendation as $item)
{{--@foreach($recomendation_single as $item)--}}

    <!--item-->

                                <div class="entry one-third">
                                    <figure>
                                        @if ($item->item_image == null) <img src="{{asset('images/no.jpg')}}"height="200" width="270" alt="restaurant">
                                        @else  <img src="{{asset('images/'.$item->item_image)}}"height="200" width="270" alt="review image">
                                        @endif
                                            <figcaption><a href="item/tag/{{$item->slug}}"><i
                                                            class="icon icon-themeenergy_eye2"></i>
                                                    <span>View post</span></a></figcaption>
                                    </figure>
                                    <div class="container">
                                        <h2>{{$item->item_name}}</h2>
                                        <div class="actions">
                                            <div>
                                                <div class="date">Created at<pre> </pre><i class="fa fa-calendar"></i>{{$item['created_at']}}</div>

                                                {{--<div class="comments"><i class="fa fa-comment"></i><a
                                                            href="blog_single.html#comments">27</a></div>--}}
                                            </div>
                                        </div>
                                        <div class="excerpt">
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                                <!--item-->
                            @endforeach


                            <div class="quicklinks">
                                <a href="/owners/all" class="button">More posts</a>
                              {{--  <a href="/item/all" class="button">More posts</a>--}}
{{--
                                <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
--}}
                            </div>
                        </div>
                        <!--//entries-->
                    </div>
                    <div class="cwrap">
                        <header class="s-title">
                            <h2 class="ribbon bright">New in Town</h2>
                        </header>
                        <!--entries-->
                        <div class="entries row">
                        @foreach($restaurants as $restaurant)
                            <!--item-->
                                <div class="entry one-third">
                                    <figure>
                                         @if ($restaurant['restaurant_logo']== null) <img src="{{asset('images/no.jpg')}}"height="200" width="270" alt="restaurant">
                                         @else  <img src="{{asset('images/'.$restaurant['restaurant_logo'])}}"height="200" width="270" alt="review image">

                                         @endif
                                        <figcaption><a href="restaurant/tag/{{$restaurant['slug']}}"><i
                                                        class="icon icon-themeenergy_eye2"></i>
                                                <span>View post</span></a></figcaption>
                                    </figure>
                                    <div class="container">
                                        <h2><a href="blog_single.html">{{$restaurant['restaurant_name']}}</a></h2>
                                        <div class="actions">
                                            <div>
                                                <h5> {{$restaurant['area']['area_name']}}</h5>
                                                <div class="date">Created at<pre> </pre><i class="fa fa-calendar"></i>{{$restaurant['created_at']}}</div>
{{--
                                                <div class="comments"><i class="fa fa-comment"></i><a
                                                            href="blog_single.html#comments">27</a></div>--}}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--item-->
                            @endforeach


                            <div class="quicklinks">
                                <a href="/restaurant/all" class="button">More posts</a>
{{--
                                <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
--}}
                            </div>
                        </div>
                        <!--//entries-->
                    </div>

                    <!--//cwrap-->
                </section>
                <!--//content-->


                <!--right sidebar-->
                <aside class="sidebar one-fourth">
                    <div class="widget">
                        <h3>Food Categories</h3>
                        <ul class="boxed">
                            <li class="light"><a href="recipes.html" title="Appetizers"><i
                                            class="icon icon-themeenergy_pasta"></i> <span>Apetizers</span></a></li>
                            <li class="medium"><a href="recipes.html" title="Storage"><i
                                            class="icon icon-themeenergy_soup2"></i> <span>Soups</span></a></li>
                            <li class="light"><a href="recipes.html" title="Snacks"><i
                                            class="icon icon-themeenergy_fried-potatoes"></i> <span>Snacks</span></a>
                            </li>


                          {{--  <li class="medium"><a href="recipes.html" title="Cocktails"><i
                                            class="icon icon-themeenergy_eggs"></i> <span>Eggs</span></a></li>
                            <li class="dark"><a href="recipes.html" title="Equipment"><i
                                            class="icon icon-themeenergy_blender"></i> <span>Equipment</span></a></li>
                            <li class="light"><a href="recipes.html" title="Events"><i
                                            class="icon icon-themeenergy_turkey"></i> <span>Events</span></a></li>--}}

                            <li class="dark"><a href="recipes.html" title="Fish"><i
                                            class="icon icon-themeenergy_fish2"></i> <span>Fish</span></a></li>
                            <li class="light"><a href="recipes.html" title="Steaks"><i
                                            class="icon icon-themeenergy_meat"></i> <span>Steaks</span></a></li>
                            <li class="dark"><a href="recipes.html" title="Pizzas"><i
                                            class="icon  icon-themeenergy_pizza-slice"></i> <span>Pizzas</span></a></li>


                            <li class="light"><a href="recipes.html" title="Asian"><i
                                            class="icon icon-themeenergy_sushi"></i> <span>Asian</span></a></li>
                            <li class="medium"><a href="recipes.html" title="Mexican"><i
                                            class="icon icon-themeenergy_peper"></i> <span>Mexican</span></a></li>
                            <li class="light"><a href="recipes.html" title="Vegetarian"><i
                                            class="icon icon-themeenergy_plant-symbol"></i> <span>Vegetarian</span></a>
                            </li>


                            <li class="dark"><a href="recipes.html" title="Salads"><i
                                            class="icon icon-themeenergy_eggplant"></i> <span>Salads</span></a></li>
                            <li class="light"><a href="recipes.html" title="Cocktails"><i
                                            class="icon icon-themeenergy_margarita2"></i> <span>Cocktails</span></a>
                            </li>
                            <li class="dark"><a href="recipes.html" title="Desserts"><i
                                            class="icon icon-themeenergy_cupcake"></i> <span>Desserts</span></a></li>


                        </ul>
                    </div>

                   {{-- <div class="widget">
                        <h3>Tips and tricks</h3>
                        <ul class="articles_latest">
                            <li>
                                <a href="blog_single.html">
                                    <img src="{{asset('/assets/images/img9.jpg')}}" alt=""/>
                                    <h6>How to decorate cookies</h6>
                                </a>
                            </li>
                            <li>
                                <a href="blog_single.html">
                                    <img src="{{asset('/assets/images/img10.jpg')}}" alt=""/>
                                    <h6>Make your own bread</h6>
                                </a>
                            </li>
                            <li>
                                <a href="blog_single.html">
                                    <img src="{{asset('/assets/images/img11.jpg')}}" alt=""/>
                                    <h6>How to make sushi</h6>
                                </a>
                            </li>
                            <li>
                                <a href="blog_single.html">
                                    <img src="{{asset('/assets/images/img12.jpg')}}" alt=""/>
                                    <h6>Barbeque party</h6>
                                </a>
                            </li>
                            <li>
                                <a href="blog_single.html">
                                    <img src="{{asset('/assets/images/img8.jpg')}}" alt=""/>
                                    <h6>How to make a cheesecake</h6>
                                </a>
                            </li>
                        </ul>
                    </div>



                    <div class="widget">
                        <h3>Advertisment</h3>
                        <a href="#"><img src="images/advertisment.jpg" alt=""/></a>
                    </div>--}}
                </aside>
            </div>
            <!--//right sidebar-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->
    <!--call to action-->
    {{-- <section class="cta">
         <div class="wrap clearfix">
             <a href="http://themeforest.net/item/socialchef-social-recipe-html-template/8568727?ref=themeenergy" class="button big white right">Purchase theme</a>
             <h2>Already convinced? This is a call to action section lorem ipsum dolor sit amet.</h2>
         </div>
     </section>--}}
    <!--//call to action-->
@endsection

@section('page_js')

    <meta name="_token" content="{!! csrf_token() !!}"/>
    <script>

        function search_list_update(id, array_list) {
            console.log(array_list);

            $(".search_result_list").html("").append("<option value=''>Select</option>"); //reset child options
            $.each(array_list, function (index, value) { //populate child options
                $(".search_result_list").append("<option value=" + index + ">" + value + "</option>");
            });
        }

        function get_infos(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            if(id ==1) {
                url = '{!! route('get_all_restaurant')  !!}';
            }if(id ==2) {
                url = '{!! route('get_areas')  !!}';
            }if(id ==3) {
                url = '{!! route('get_items')  !!}';
            }

            $.ajax({
                type: "Post",
                url: url,
                data: {id: id},
                success: function (data) {
                    obj = JSON.parse(data);
                    output = obj[0];
//                    console.log(obj);
                    $('.search_result_list').empty().val(null);
                    search_list_update(id,output);
                }
            })

        }

        function get_details(id,result_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            if(id ==1) {
                url = '{!! route('all_restaurant')  !!}';
            }if(id ==2) {
                url = '{!! route('all_areas')  !!}';
            }
            {{--}if(id ==3) {--}}
                {{--url = '{!! route('get_items')  !!}';--}}
            {{--}--}}

            $.ajax({
                type: "Post",
                url: url,
                data: {id: result_id},
                success: function (data) {
                    obj = JSON.parse(data);
                    output = obj[0][0];
                    console.log(obj);
                    if(id=1) {
                        var g=output['slug'];
                        var lin="http://localhost:8000/restaurant/tag/"+g;
                        var elem = "<p>" + output['restaurant_name'] + "</p>";
                        elem += "<p>" + output['address'] + "</p>";
                     /*   elem += "<p>" + lin + "</p>";*/
                        elem +="<a href="+lin+">Read More</a>";


                        $("#searched_data").html(elem);
                    }
                  /*  if(id=2){
                        var elem = "<p>" + output['id'] + "</p>";
                        elem += "<p>" + output['restaurant'][0]['restaurant_name'] + "</p>";

                         elem += "<p>" + output['id']+ "</p>";
                        elem += "<p>" + output['restaurant'][1]['restaurant_name'] + "</p>";
                        $("#searched_data").html(elem);
                       /!* alert(output['restaurant'][0]['restaurant_name']);*!/
                    }*/

                }
            })

        }

        $(function () {
            $('.search_item,.search_result_list').select2({
                placeholder: 'Select an option',

            });

            $('.search_item').on('change', function () {
                searched_id = $('.search_item').val();
//                console.log(restaurant_id);
                get_infos(searched_id);

            })
            $('.search_result_list').on('change', function () {
                searched_id = $('.search_item').val();
                search_result_id = $('.search_result_list').val();
//                console.log(restaurant_id);
                get_details(searched_id,search_result_id);

            })
        })
    </script>

@endsection

