@extends('main')
@section('content')

    <div class="wrap clearfix">
        <!--row-->
        <div class="row"style="height:550px">
            <!--content-->
            <section class="content three-fourth">
                <!--row-->
                <div class="row">
                    <div class="one-third">
                        <div class="error-container">
                            <span class="error_type">404</span>
                            <span class="error_text">Page not found</span>
                        </div>
                    </div>

                    <div class="two-third">
                        <div class="container">
                            <p>You can't review same thing twice </p>

                            <p>You can go <a href="#">back home</a> </p>
                        </div>
                    </div>
                </div>
                <!--//row-->
            </section>
            <!--//content-->

         {{--   <!--sidebar-->
            <aside class="sidebar one-fourth">
                <div class="widget">
                    <h3>Search</h3>
                    <div class="f-row">
                        <input type="search" placeholder="Enter your search term" />
                    </div>
                    <div class="f-row">
                        <input type="submit" value="Search" />
                    </div>
                </div>
            </aside>
            <!--//sidebar-->--}}
        </div>
        <!--//row-->
    </div>
    <!--//main-->
    <!--call to action-->
    {{-- <section class="cta">
         <div class="wrap clearfix">
             <a href="http://themeforest.net/item/socialchef-social-recipe-html-template/8568727?ref=themeenergy" class="button big white right">Purchase theme</a>
             <h2>Already convinced? This is a call to action section lorem ipsum dolor sit amet.</h2>
         </div>
     </section>--}}
    <!--//call to action-->
@endsection

@section('page_js')

    <meta name="_token" content="{!! csrf_token() !!}"/>
    <script>

        function search_list_update(id, array_list) {
            console.log(array_list);

            $(".search_result_list").html("").append("<option value=''>Select</option>"); //reset child options
            $.each(array_list, function (index, value) { //populate child options
                $(".search_result_list").append("<option value=" + index + ">" + value + "</option>");
            });
        }

        function get_infos(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            if(id ==1) {
                url = '{!! route('get_all_restaurant')  !!}';
            }if(id ==2) {
                url = '{!! route('get_areas')  !!}';
            }if(id ==3) {
                url = '{!! route('get_items')  !!}';
            }

            $.ajax({
                type: "Post",
                url: url,
                data: {id: id},
                success: function (data) {
                    obj = JSON.parse(data);
                    output = obj[0];
//                    console.log(obj);
                    $('.search_result_list').empty().val(null);
                    search_list_update(id,output);
                }
            })

        }

        function get_details(id,result_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            if(id ==1) {
                url = '{!! route('all_restaurant')  !!}';
            }if(id ==2) {
                url = '{!! route('all_areas')  !!}';
            }
            {{--}if(id ==3) {--}}
                {{--url = '{!! route('get_items')  !!}';--}}
            {{--}--}}

            $.ajax({
                type: "Post",
                url: url,
                data: {id: result_id},
                success: function (data) {
                    obj = JSON.parse(data);
                    output = obj[0][0];
                    console.log(obj);
                    if(id=1) {
                        var g=output['slug'];
                        var lin="http://localhost:8000/restaurant/tag/"+g;
                        var elem = "<p>" + output['restaurant_name'] + "</p>";
                        elem += "<p>" + output['address'] + "</p>";
                     /*   elem += "<p>" + lin + "</p>";*/
                        elem +="<a href="+lin+">Read More</a>";


                        $("#searched_data").html(elem);
                    }
                  /*  if(id=2){
                        var elem = "<p>" + output['id'] + "</p>";
                        elem += "<p>" + output['restaurant'][0]['restaurant_name'] + "</p>";

                         elem += "<p>" + output['id']+ "</p>";
                        elem += "<p>" + output['restaurant'][1]['restaurant_name'] + "</p>";
                        $("#searched_data").html(elem);
                       /!* alert(output['restaurant'][0]['restaurant_name']);*!/
                    }*/

                }
            })

        }

        $(function () {
            $('.search_item,.search_result_list').select2({
                placeholder: 'Select an option',

            });

            $('.search_item').on('change', function () {
                searched_id = $('.search_item').val();
//                console.log(restaurant_id);
                get_infos(searched_id);

            })
            $('.search_result_list').on('change', function () {
                searched_id = $('.search_item').val();
                search_result_id = $('.search_result_list').val();
//                console.log(restaurant_id);
                get_details(searched_id,search_result_id);

            })
        })
    </script>

@endsection

