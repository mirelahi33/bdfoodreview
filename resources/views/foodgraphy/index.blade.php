@extends('main')

@section('content')

    <div class="row">
       <div class="col-md-10">

           <h1>all post</h1>

       </div>
        <div class="col-md-2">

            <a href="{{ route('reviews.create') }}" class="btn btn-primary btn-block">Give a review</a>

        </div>

<div class="row">
    <div class="col-md-12">
        <table class="table">

            <thead>
            <th>#</th>
            <th>reviewer name</th>
            <th>restaurant name</th>
            <th>Place</th>
            <th>Name of Food</th>
            <th>Rating</th>
            <th>Comment</th>

            </thead>
<tbody>
  @foreach($reviews as $review)
<tr>
    <th>{{$review->id}}</th>
    <td>{{$review->reviewer_name}}</td>
    <td>{{$review->restaurant->restaurant_name}}</td>
    <td>{{$review->place}}</td>
    <td>{{$review->name_of_food}}</td>
    <td>{{$review->rating}}</td>
    <td>{{$review->comment}}</td>


    {{--<td>{{str_limit($restaurant->address, 20)}}</td>
    <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
    <td>  <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
                --}}{{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}{{--
                <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿</td>--}}
</tr>
@endforeach

</tbody>
        </table>


    </div>

</div>

    </div>﻿

    @endsection