@extends('main')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Write a review</h1>
            <hr>
            <form method="POST" action="{{ route('reviews.store') }}">
                <div class="form-group">
                    <label name="reviewer_name">Reviewer:</label>
                    <input id="reviewer_name" name="reviewer_name" class="form-control">
                </div>
                <div class="form-group">
                    <label name="restaurant_name">Restaurant name:</label>
                    <select name="restaurant_name" id="restaurant_name" class="form-control">
                        <option value=""> -- Select One --</option>
                        @foreach ($x as $restaurant_name)
                            <option value="{{ $restaurant_name->id }}">{{ $restaurant_name->restaurant_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label name="restaurant_name">Place:</label>
                    <input id="place" name="place" class="form-control">
                </div>
                <div class="form-group">
                    <label name="restaurant_name">Name of food:</label>
                    <input id="name_of_food" name="name_of_food" class="form-control">
                </div>
                <div class="form-group">
                    <label name="restaurant_name">rating:</label>
                    <input id="rating" name="rating" class="form-control">
                </div>
                <div class="form-group">
                    <label name="address">comment:</label>
                    <textarea id="comment" name="comment" rows="10" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted"></small>
                </div>
                <input type="submit" value="give review" class="btn btn-success btn-lg btn-block">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </div>﻿

    @endsection