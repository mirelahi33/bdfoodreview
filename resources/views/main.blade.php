<!DOCTYPE html>
<html>

<head>
    @include('partials2._head')
</head>

<body>
@include(('partials2._nav'))



    @yield('content')


    @include('partials2._messages')


    @include('partials2._footer')
@include('partials2._javascript')

@yield('scripts')


@yield('page_js')
</body>

</html>