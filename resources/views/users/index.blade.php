@extends('main')

@section('content')

    <div class="row">
       <div class="col-md-10">

           <h1>all users</h1>

       </div>
      {{--  <div class="col-md-2">

            <a href="{{ route('users.create') }}" class="btn btn-primary btn-block">Give a review</a>

        </div>--}}

<div class="row">
    <div class="col-md-12">
        <table class="table" id="users">

            <thead>
            <th>user name</th>
            <th>email</th>
            <th>user role</th>
            <th>edit role</th>


            </thead>
<tbody>
  @foreach($users as $user)
<tr>
    <td>{{$user->name}}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->user_type}}</td>


          <td>
    <div class="col-sm-4">
        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-success">Edit</a>
    </div>

        ﻿</td>
    {{--<td>{{str_limit($restaurant->address, 20)}}</td>
    <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
    <td>  <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
                --}}{{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}{{--
                <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿</td>--}}
</tr>
@endforeach

</tbody>
        </table>


    </div>

</div>

    </div>﻿

    @endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#users').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection