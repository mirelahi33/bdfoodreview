<div class="preloader">
    <div class="spinner"></div>
</div>

<header class="head" role="banner">
    <!--wrap-->
    <div class="wrap clearfix">
        <a href="/home" title="SocialChef" class="logo"><img src="{{asset('assets/images/ico/logo.png')}}"  alt="BdFoodReview logo "/></a>

        <nav class="main-nav" role="navigation" id="menu">
            <ul>

                <li {{--class="current-menu-item"--}}><a href="/home" title="Home"><span>Home</span></a></li>
                <li><a href="/reviews" title=""><span>Reviews</span></a>
                    <ul>
                        @can('isUser')

                            <li><a href="/reviews/create" title="Recipe">Give a review</a></li>
                        @endcan

                        <li><a href="/reviews" title="Recipes 2">All reviews</a></li>

                        <li><a href="/reviews/search" title="Blog post">search in review</a></li>

                    </ul>
                </li>
                <li><a href="/shareafood" title=""><span>Share a Food</span></a>
                    <ul>
                        @can('isUser')
                        <li><a href="/shareafood/create" title="Blog post">Share a food request</a>
                        @endcan
                        <li><a href="/shareafood" title="Blog post">show Share a Food</a></li>


                        <li><a href="/shareafood/search" title="Blog post">search in share a food</a></li>


                    </ul>
                </li>
                <li><a href="/offer" title=""><span>Offers</span></a>
                    <ul>

                        <li><a href="/offer" title="Blog post">show offers</a></li>
                        <li><a href="/offer/search" title="Blog post">search in offers</a></li>
                    </ul>
                </li>
                <li><a href="/map" title="Features"><span>Map</span></a>

                </li>
                <li><a href="/foodgraphy" title="foodgraphy"><span>Foodgraphy</span></a></li>

                @if(Gate::check('isSuperAdmin') || Gate::check('isRestaurantModerator'))  {{--setiings grom AuthServiceProvider--}}

                <li><a href="{{--/admin_create--}}" title="Pages"><span>Create</span></a>
                    <ul>

                        <li><a href="/restaurant" title="Blog post">Restaurant</a></li>
                        <li><a href="/item" title="Blog post">Items</a></li>
                        <li><a href="/offer/create" title="Blog post">Offer</a></li>

                        @can('isSuperAdmin')
                            <li><a href="/area" title="Blog post">Add Area</a></li>
                        <li><a href="/shareafood" title="Blog post">Moderate Share a Food</a></li>
                        <li><a href="/reviews" title="Blog post">Moderate Reviews</a></li>
                        <li><a href="/users" title="Blog post">Moderate User</a></li>

                           @endcan

                    </ul>
                </li>
                @endif
            </ul>



            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest


                <li><a href="{{ route('login') }}"><pre>Login </pre></a></li>
                <li><a href="{{ route('register') }}">Register</a></li>

                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                           aria-haspopup="true">
                            {{ Auth::user()->name }} <span class=""></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                    @endguest

            </ul>
        </nav>


        <nav class="user-nav" role="navigation">
            <ul>
                <li class="light"><a href="/shareafood/create" title="Search for recipes"><i class="icon icon-themeenergy_burger-coke"></i> <span>Share a Food</span></a></li>
{{--
                <li class="medium"><a href="my_profile.html" title="My account"><i class="icon icon-themeenergy_chef-hat"></i> <span>My account</span></a></li>
--}}
                <li class="dark"><a href="/reviews/create" title="Submit a recipe"><i class="icon icon-themeenergy_star-2"></i> <span>Give a review</span></a></li>
            </ul>
        </nav>

    </div>
    <!--//wrap-->
</header>