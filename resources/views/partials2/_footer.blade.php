<style>

    #back2Top {
        width: 40px;
        line-height: 40px;
        overflow: hidden;
        z-index: 999;
        display: none;
        cursor: pointer;
        -moz-transform: rotate(270deg);
        -webkit-transform: rotate(270deg);
        -o-transform: rotate(270deg);
        -ms-transform: rotate(270deg);
        transform: rotate(270deg);
        position: fixed;
        bottom: 50px;
        right: 0;
        background-color: #DDD;
        color: #555;
        text-align: center;
        font-size: 30px;
        text-decoration: none;
    }
    #back2Top:hover {
        background-color: #DDF;
        color: #000;
    }

    footer {
/*
        position: fixed;
*/
        left: 0;
        bottom: 0;
        height: 10%;
        width: 100%;
        background-color: white;
    }
</style>

<!--footer-->
<footer class="" role="contentinfo">

        <div class="row">
            <a id="back2Top" title="Back to top" href="#">&#10148;</a>

            <div class="bottom" style="padding-left:20px;">
                <p class="copy">Copyright 2018 BdFoodReview. All rights reserved</p>

                <nav class="foot-nav" style="padding-right:20px; ">
                    <ul>
                        <li><a href="/home" title="Home">Home</a></li>
                        <li><a href="/restaurant/all" title="Recipes">Restaurants</a></li>
                        <li><a href="/item/all" title="Blog">Items</a></li>
                        <li><a href="/contact" title="Contact">Contact</a></li>
                        <li><a href="/about" title="Search for recipes">About Us</a></li>
                      @guest  <li><a href="/login" title="Login">Login</a></li>	<li><a href="/register" title="Register">Register</a></li> @endguest
                    </ul>
                </nav>
            </div>
        </div>
</footer>
<!--//footer-->

