@extends('main')

@section('content')
    <div class="row">
        <!--content-->
        <section class="content center full-width">
            <div class="modal container">
                <form  method="POST" enctype="multipart/form-data" action="{{ route('reviews.store') }}" {{--name="contactform" id="contactform"--}}>
                    <h1 class="center">Write a Review</h1>
                    <div id="message" class="alert alert-danger"></div>
                    <div class="f-row">
{{--
                        <input type="text" placeholder="Your name" id="name" />
--}}
                        <label name="restaurant_name">Restaurant name:</label>
                        <select name="restaurant_name" style="width:50%" id="restaurant_name" class="form-control" required="">
                            <option value=""> -- Select One --</option>
                            @foreach ($x as $restaurant_name)
                                <option value="{{ $restaurant_name->id }}">{{ $restaurant_name->restaurant_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="f-row">
                        <label name="area_name">Restaurant area:</label>
                        <select name="area_name" style="width:50%" id="area_name" class="form-control" required>
                            <option value=""> -- Select One --</option>
                            @foreach ($y as $area_name)
                                <option value="{{ $area_name->id }}">{{ $area_name->area_name }}</option>
                            @endforeach
                        </select>
{{--
                        <input type="email" placeholder="Your email" id="email" />
--}}
                    </div>

                    <div class="f-row">
                        <label name="item_name">item name:</label>
                        <select name="item_name" style="width:50%" id="item_name" class="form-control" required>
                            <option value=""> -- Select One --</option>
                            @foreach ($z as $item_name)
                                <option value="{{ $item_name->id }}">{{ $item_name->item_name }}</option>
                            @endforeach
                        </select>
                        {{--<input type="number" placeholder="Your phone number" id="phone" />--}}
                    </div>

                    <div class="f-row">
{{--
                        <label name="rating">rating:</label>
--}}
                        <input id="rating" name="rating"   type="number" class="form-control" placeholder="Rating" required>
                    </div>
                    <div class="f-row">
                        <label class="col-md-3 control-label" >image:</label>
                        <div class="col-md-8">
                            <input  type="file" id="featured_img" placeholder="featured_img" name="featured_img" class=""/>
                            <span class="required" id='spnFileError'></span>
                        </div>
                    </div>


                    <div class="f-row">
                        <textarea placeholder="comment" id="comment" name="comment" required></textarea>
                    </div>
                    <div class="f-row bwrap">
                        <input type="submit" value="Send review" />
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
            </div>
        </section>
        <!--//content-->
    </div>
@endsection
@section('page_js')

    <meta name="_token" content="{!! csrf_token() !!}"/>
    <script>

        var url = '{!! route('get_restaurant_infos')  !!}';

        function search_list_update(name, array_list) {
            console.log(array_list);
            $("#" + name + "").html("").append("<option value=''>Select</option>"); //reset child options
            $.each(array_list, function (index, value) { //populate child options
                $("#" + name + "").append("<option value=" + index + ">" + value + "</option>");
            });
        }
        function get_restaurants_info(restaurant_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            console.log(restaurant_id);
            console.log(url);
            $.ajax({
                type: "Post",
                url: url,
                data: {restaurant_id: restaurant_id},
                success: function (data) {
                    obj = JSON.parse(data);
                    output = obj[0];
                    //   output1 = obj[1];
                    console.log(obj);
                    /*    $('#item_name').empty().val(null);
                     $('#area_name').empty().val(null);
                     $('#offer_name').empty().val(null);*/
                    Item = "item_name";
                    Offer = "offer_name";
                    Area = "area_name";
                    search_list_update(Area, obj[0]);
                    //    search_list_update(Item, obj[1]);
//                    search_list_update(Offer, obj['ManufacturerProducts']);


                }
            })


            $.ajax({
                type: "Post",
                url: url,
                data: {restaurant_id: restaurant_id},
                success: function (data) {
                    obj = JSON.parse(data);
                    output1 = obj[1];
                    //   output1 = obj[1];
                    console.log(obj);
                    //        $('#item_name').empty().val(null);
                    //      $('#area_name').empty().val(null);
                    //      $('#offer_name').empty().val(null);
                    Item = "item_name";
                    Offer = "offer_name";
                    Area = "area_name";
                    search_list_update(Item, obj[1]);
                    //    search_list_update(Item, obj[1]);
//                    search_list_update(Offer, obj['ManufacturerProducts']);


                }
            })

            $.ajax({
                type: "Post",
                url: url,
                data: {restaurant_id: restaurant_id},
                success: function (data) {
                    obj = JSON.parse(data);
                    output2 = obj[2];
                    //   output1 = obj[1];
                    console.log(obj);
                    //        $('#item_name').empty().val(null);
                    //      $('#area_name').empty().val(null);
                    //      $('#offer_name').empty().val(null);
                    Item = "item_name";
                    Offer = "offer_name";
                    Area = "area_name";
                    // search_list_update(Item, obj[1]);
                    //    search_list_update(Item, obj[1]);
                    search_list_update(Offer, obj[2]);


                }
            })

        }

        $(function () {
            $('#restaurant_name,#item_name,#area_name,#offer_name').select2({
                placeholder: 'Select an option'
            });

            $('#restaurant_name').on('change', function () {
                restaurant_id = $('#restaurant_name').val();
                console.log(restaurant_id);
                get_restaurants_info(restaurant_id);
            })
        })
    </script>

@endsection

{{--

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Write a review</h1>
        <hr>
        <form method="POST" action="{{ route('reviews.store') }}">
            --}}{{--  <div class="form-group">
                  <label name="reviewer_name">Reviewer:</label>
                  <input id="reviewer_name" name="reviewer_name" class="form-control">
              </div>--}}{{--
            <div class="form-group">
                <label name="restaurant_name">Restaurant name:</label>
                <select name="restaurant_name" id="restaurant_name" class="form-control">
                    <option value=""> -- Select One --</option>
                    @foreach ($x as $restaurant_name)
                        <option value="{{ $restaurant_name->id }}">{{ $restaurant_name->restaurant_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label name="area_name">Restaurant area:</label>
                <select name="area_name" id="area_name" class="form-control">
                    <option value=""> -- Select One --</option>
                    @foreach ($y as $area_name)
                        <option value="{{ $area_name->id }}">{{ $area_name->area_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label name="item_name">item name:</label>
                <select name="item_name" id="item_name" class="form-control">
                    <option value=""> -- Select One --</option>
                    @foreach ($z as $item_name)
                        <option value="{{ $item_name->id }}">{{ $item_name->item_name }}</option>
                    @endforeach
                </select>
            </div>
            --}}{{-- <div class="form-group">
                 <label name="restaurant_name">Place:</label>
                 <input id="place" name="place" class="form-control">
             </div>
             <div class="form-group">
                 <label name="restaurant_name">Name of food:</label>
                 <input id="name_of_food" name="name_of_food" class="form-control">
             </div>--}}{{--
            <div class="form-group">
                <label name="restaurant_name">rating:</label>
                <input id="rating" name="rating" class="form-control">
            </div>
            <div class="form-group">
                <label name="address">comment:</label>
                <textarea id="comment" name="comment" rows="10" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" name="review_image">review image:</label>
                <div class="col-md-8">
                    <input  type="file" id="review_image" placeholder="review_image" name="review_image" class=""/>
                    <span class="required" id='spnFileError'></span>
                </div>
            </div>



            <input type="submit" value="give review" class="btn btn-success btn-lg btn-block">
            <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
</div>﻿--}}

