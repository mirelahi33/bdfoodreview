@extends('main')

@section('content')
    <div class="wrap clearfix">
        <!--breadcrumbs-->
        <nav class="breadcrumbs">
            <ul>
                <li><a href="/home" title="Home">Home</a></li>
                <li>Reviews</li>
            </ul>
        </nav>
        <!--//breadcrumbs-->

        <!--row-->
        <div class="row">
            <header class="s-title">
                <h1>Reviews</h1>
            </header>

            <!--content-->
            <section class="content three-fourth">
                <!--entries-->
                <div class="entries row">
                    <!--item-->
                    @foreach ($reviews as $review)
                    <div class="entry one-third">
                        <figure>
                            @if ($review->review_image==null) <img src="{{asset('images/no.jpg')}}"height="200" width="270" alt="review image">
                          @else  <img src="{{asset('images/'.$review->review_image)}}"height="200" width="270" alt="review image">

                        @endif
                            <figcaption><a href="{{ route('reviews.single', $review->slug) }}"><i class="icon icon-themeenergy_eye2"></i> <span>View recipe</span></a></figcaption>
                        </figure>
                        <div class="container">
                            <h2>{{ substr(strip_tags( $review->item->item_name), 0,20) }}{{ strlen(strip_tags( $review->item->item_name)) > 20 ? '...' : "" }}</h2>

{{--
                            <h2>{{ $review->item->item_name }}</h2>
--}}
                            <p>Published: {{ date('M j, Y', strtotime($review->created_at)) }}</p>

                            <p>{{ substr(strip_tags($review->comment), 0, 30) }}{{ strlen(strip_tags($review->comment)) > 30 ? '...' : "" }}</p>

                            <a href="{{ route('reviews.single', $review->slug) }}" class="button">Read More</a>

                           {{-- <h2><a href="recipe.html">Thai fried rice with fruit and vegetables</a></h2>--}}

                        </div>
                    </div>
                    @endforeach



                </div>
                <!--//entries-->
            </section>
            <!--//content-->

            <!--right sidebar-->
            <aside class="sidebar one-fourth">
                <div class="widget">
                    <ul class="categories right">
                        <li class="active"><a href="#">All Catagories</a></li>
                        <li><a href="#">Apetizers</a></li>
                        <li><a href="#">Soups</a></li>
                        <li><a href="#">Snacks</a></li>
                        <li><a href="#">Fish</a></li>
                        <li><a href="#">Steaks</a></li>
                        <li><a href="#">Pizzas</a></li>
                        <li><a href="#">Asian</a></li>
                        <li><a href="#">Mexican</a></li>
                        <li><a href="#">Vegetarian</a></li>
                        <li><a href="#">Salads</a></li>
                        <li><a href="#">Cocktails</a></li>
                        <li><a href="#">Desserts</a></li>
                    </ul>
                </div>

            </aside>
            <!--//right sidebar-->
        </div>
        <!--//row-->
    </div>




    @endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#reviews').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection




{{--


<div class="row">
    <div class="col-md-10">

        <h1>all reviews</h1>

    </div>
    <div class="col-md-2">

        <a href="{{ route('reviews.create') }}" class="btn btn-primary btn-block">Give a review</a>

    </div>


    @foreach ($reviews as $review)
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2>{{ $review->reviewer_name }}</h2>
                <h5>Published: {{ date('M j, Y', strtotime($review->created_at)) }}</h5>

                <p>{{ substr(strip_tags($review->comment), 0, 250) }}{{ strlen(strip_tags($review->comment)) > 250 ? '...' : "" }}</p>

                <a href="{{ route('reviews.single', $review->slug) }}" class="btn btn-primary">Read More</a>
                <hr>
            </div>
        </div>
    @endforeach


    --}}
{{--<div class="row">
        <div class="col-md-12">
            <table class="table" id="reviews">

                <thead>
                <th>#</th>
                <th>reviewer name</th>
                <th>restaurant name</th>
                <th>Place</th>
                <th>Name of Food</th>
                <th>Rating</th>
                <th>Comment</th>
                <th>more</th>

                </thead>
    <tbody>
      @foreach($reviews as $review)
    <tr>
        <th>{{$review->id}}</th>
        <td>{{$review->reviewer_name}}</td>
        <td>{{$review->restaurant->restaurant_name}}</td>
        <td>{{$review->area->area_name}}</td>
       <td>{{$review->item->item_name}}</td>

        <td>{{$review->rating}}</td>
        <td>{{$review->comment}}</td>

    <td> --}}{{--
--}}
{{--<a href="{{ route('reviews.single',$review->slug) }}" class="btn btn-primary">Read More</a>--}}{{--
--}}
{{--
        <form method="post" action="{{route('reviews.single',$review->slug)}}">
            <input type="hidden" name="ff" value="aa"/>

            <button class=" btn  btn-success " type="submit">
               read more
            </button>
            {{ csrf_field() }}
        </form>
    </td>
        --}}{{--
--}}
{{--<td>{{str_limit($restaurant->address, 20)}}</td>
        <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
        <td>  <div class="row">
                <div class="col-sm-4">
                    <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
                </div>
                <div class="col-sm-4">
                    <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
                </div>
                <div class="col-sm-4">
                    --}}{{--
--}}
{{----}}{{--
--}}
{{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}{{--
--}}
{{----}}{{--
--}}
{{--
                    <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                        <input type="submit" value="Delete" class="btn btn-danger btn-block">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        {{ method_field('DELETE') }}
                    </form>﻿
                </div>
            </div>﻿</td>--}}{{--
--}}
{{--
    </tr>
    @endforeach

    </tbody>
            </table>


        </div>

    </div>--}}{{--


</div>﻿--}}
