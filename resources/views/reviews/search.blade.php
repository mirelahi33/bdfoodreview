@extends('main')

@section('content')



    <div class="row">
        <div class="col-md-12">
            <table class="table" id="reviews">

                <thead>
                <th>reviewer name</th>
                <th>review image</th>
                <th>restaurant name</th>

                <th>Place</th>
                <th>Name of Food</th>
                <th>Rating</th>
                <th>Comment</th>
                <th>more</th>

                </thead>
                <tbody>

                @foreach($reviews as $review)

                    <tr>
                        <td>{{$review->reviewer_name}}</td>
                        @if ($review->review_image==null)  <td> <img src="{{asset('/images/no.jpg')}}" alt="restaurant_logo" height="200" width="200"> </td>
                        @else  <td><img src="{{asset('/images/'.$review->review_image)}}" alt="restaurant_logo" height="200" width="200"></td>
                        @endif



                        <td>{{$review->restaurant->restaurant_name}}</td>


                        {{--
                        <td>{{$review->restaurant->restaurant_name}}</td>
--}}
                        <td>{{$review->area->area_name}}</td>
                        <td>{{$review->item->item_name}}</td>

                        <td>{{$review->rating}}</td>
                        <td>{{$review->comment}}</td>

                        <td>
                            <a href="{{ route('reviews.single',$review->slug) }}" class="btn btn-primary">Read More</a>



                    </tr>
                @endforeach

                </tbody>
            </table>


        </div>

    </div>



    @endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#reviews').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection




