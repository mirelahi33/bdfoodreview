@extends('main')

@section('content')
    <div class="row">
        <!--content-->
        <section class="content center full-width">
            <div class="modal container">
                <form  method="POST" action="{{ route('area.store') }}" {{--name="contactform" id="contactform"--}}>
                    <h1 class="center">Add an area</h1>
                    <div id="message" class="alert alert-danger"></div>
                    <div class="f-row">
                        <label name="reviewer_name">Area Name:</label>
                        <input type="text" id="area_name" name="area_name" class="form-control" required="">
                    </div>


                    <div class="f-row bwrap">
                        <input type="submit" value="Add an Area" />
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
            </div>
        </section>
        <!--//content-->
    </div>


    @endsection

{{--
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Write a review</h1>
        <hr>
        <form method="POST" action="{{ route('area.store') }}">
            <div class="form-group">
                <label name="reviewer_name">Area Name:</label>
                <input id="area_name" name="area_name" class="form-control">
            </div>

            <input type="submit" value="Submit" class="btn btn-success btn-lg btn-block">
            <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
</div>﻿--}}
