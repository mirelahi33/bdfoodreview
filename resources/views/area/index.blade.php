@extends('main')

@section('content')

    <div class="row">
       <div class="col-md-10">

           <h1>all post</h1>

       </div>
        <div class="col-md-2">

            <a href="{{ route('area.create') }}" class="button">create an area</a>

        </div>

<div class="row">
    <div class="col-md-12">
        <table class="table" id="areas">

            <thead>
{{--
            <th>#</th>
--}}
            <th>area name</th>


            </thead>
<tbody>
  @foreach($area as $areas)
<tr>
{{--
    <td>{{$areas->id}}</td>
--}}
    <td>{{$areas->area_name}}</td>



    {{--<td>{{str_limit($restaurant->address, 20)}}</td>
    <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
    <td>  <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
                --}}{{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}{{--
                <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿</td>--}}
</tr>
@endforeach

</tbody>
        </table>


    </div>

</div>

    </div>﻿

    @endsection


@section('page_js')
    <script>
        $(function () {
            var table = $('#areas').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection