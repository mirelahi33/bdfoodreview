@extends('main')

@section('content')


        <div class="center">

            <a href="{{ route('shareafood.create') }}"  class="button">Add share a food</a>

        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table" id="shareafoods">

                    <thead>

                    <th>restaurant name</th>
                    <th>item name</th>
                    <th>Offer name</th>
                    <th>Area name</th>
                    <th>No of person</th>
                    <th>Estimate Date</th>
                    <th>Estimate time</th>
                    <th>Details</th>
                    <th>image</th>


                    </thead>
                    <tbody>


                      @foreach($shareafood as $share)


                          <tr>
                        <td>{{$share->restaurant->restaurant_name}}</td>
                        <td>{{$share->item->item_name}}</td>
                    @if($share->offer)   <td>{{$share->offer->offer_name}}</td>
                        @else <td></td>
                        @endif
                      {{--  {{dd($share->offer->offer_name)}}--}}

                        <td>{{$share->area->area_name}}</td>
                        <td>{{$share->no_of_person}}</td>
                        <td>{{$share->estimate_date}}</td>
                        <td>{{$share->estimate_time}}</td>
                        <td>{{$share->shareafood_caption}}</td>
                        @if ($share->shareafood_image==null)  <td></td>
                        @else  <td><img src="{{asset('/images/'.$share->shareafood_image)}}" alt="shareafood_image" height="200" width="200"></td>@endif


                        {{--<td>{{str_limit($restaurant->address, 20)}}</td>
                        <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
                        <td>  <div class="row">
                                <div class="col-sm-4">
                                    <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
                                </div>
                                <div class="col-sm-4">
                                    <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
                                </div>
                                <div class="col-sm-4">
                                    --}}{{----}}{{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}{{----}}{{--
                                    <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                                        <input type="submit" value="Delete" class="btn btn-danger btn-block">
                                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                                        {{ method_field('DELETE') }}
                                    </form>﻿
                                </div>
                            </div>﻿</td>--}}
                    </tr>
                    @endforeach

                    </tbody>
                </table>


            </div>

        </div>



@endsection


@section('page_js')
    <script>
        $(function () {
            var table = $('#shareafoods').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection