@extends('main')


@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		{{--	@if(!empty($reviews->image))
				<img src="{{asset('/images/' . $post->image)}}" width="800" height="400" />
			@endif--}}
			<div class="row">
				<div class="col-md-12">
					<table class="table" id="reviews">

						<thead>

						<th>restaurant name</th>
						<th>item name</th>
						<th>Offer name</th>
						<th>Area name</th>
						<th>No of person</th>
						<th>Estimate Date</th>
						<th>Estimate time</th>
						<th>Details</th>
						<th>image</th>



						</thead>
						<tbody>

							<tr>

								<td>{{$shareafood->restaurant->restaurant_name}}</td>
								<td>{{$shareafood->item->item_name}}</td>
								@if($shareafood->offer)   <td>{{$shareafood->offer->offer_name}}</td>
								@else <td></td>
								@endif
								{{--  {{dd($share->offer->offer_name)}}--}}

								<td>{{$shareafood->area->area_name}}</td>
								<td>{{$shareafood->no_of_person}}</td>
								<td>{{$shareafood->estimate_date}}</td>
								<td>{{$shareafood->estimate_time}}</td>
								<td>{{$shareafood->shareafood_caption}}</td>
								@if ($shareafood->shareafood_image==null)  <td></td>
								@else  <td><img src="{{asset('/images/'.$shareafood->shareafood_image)}}" alt="shareafood_image" height="200" width="200"></td>@endif



                            </tr>


</tbody>
        </table>


    </div>

</div>


			{{--<p>{!! $post->body !!}</p>
			<hr>
			<p>Posted In: {{ $post->category->name }}</p>--}}
		</div>
	</div>



	<div class="comments" id="comments">
		<h2>{{ $shareafood->shareafoodcomments()->count() }} Comments </h2>
		@foreach($shareafood->shareafoodcomments as $comment)

               {{--         {{dd($comment->user['user_image'])}}--}}

			<ol class="comment-list">


				<!--comment-->
				<li class="comment depth-1">

					<div class="avatar"><a href="my_profile.html"><img src="{{asset('images/'.$comment->user['user_image'])}}" alt="" /></a></div>

				{{--	@if ($comment->user['user_image']==null)  <td> <img src="{{asset('images/no.jpg')}}" alt="" /> </td>
					@else  <td><img src="{{asset('/images/'.$comment->user['user_image'])}}" alt="restaurant_logo" height="200" width="200"></td>@endif--}}


					<div class="comment-box" style="text-align: left">
						<div class="comment-author meta">
							Name:	<strong>{{ $comment->name}}</strong> {{ date('F dS, Y - g:iA' ,strtotime($comment->created_at)) }} {{--<a href="#" class="comment-reply-link"> Reply</a>--}}
						</div>
						<div class="comment-text">
							{{ $comment->comment }}
						</div>
					</div>
				</li>
				<!--//comment-->


			</ol>
		@endforeach
	</div>


{{--
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3 class="comments-title"><span class="glyphicon glyphicon-comment"></span>  {{ $shareafood->shareafoodcomments()->count() }} Comments</h3>
			@foreach($shareafood->shareafoodcomments as $comment)
				<div class="comment">
					<div class="author-info">

						--}}
{{--<img src="{{ "https://www.gravatar.com/avatar/" . md5(strtolower(trim($comment->email))) . "?s=50&d=monsterid" }}" class="author-image">--}}{{--

						<div class="author-name">

							<h4>{{ $comment->name}}</h4>
							<p class="author-time">{{ date('F dS, Y - g:iA' ,strtotime($comment->created_at)) }}</p>
						</div>

					</div>

					<div class="comment-content">
						{{ $comment->comment }}
					</div>

				</div>
			@endforeach
		</div>
	</div>
--}}


	{{--<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3 class="comments-title"><span class="glyphicon glyphicon-comment"></span>  {{ $post->comments()->count() }} Comments</h3>
			@foreach($post->comments as $comment)
				<div class="comment">
					<div class="author-info">

						<img src="{{ "https://www.gravatar.com/avatar/" . md5(strtolower(trim($comment->email))) . "?s=50&d=monsterid" }}" class="author-image">
						<div class="author-name">
							<h4>{{ $comment->name }}</h4>
							<p class="author-time">{{ date('F dS, Y - g:iA' ,strtotime($comment->created_at)) }}</p>
						</div>

					</div>

					<div class="comment-content">
						{{ $comment->comment }}
					</div>

				</div>
			@endforeach
		</div>
	</div>--}}

	<div class="comment-respond" id="respond">
		<h2>Leave a reply</h2>
		<div class="container">
			<p><strong>Note:</strong> Comments on the web site reflect the views of their authors, and not necessarily the views of the BdFoodReview internet portal. Requested to refrain from insults, swearing and vulgar expression. We reserve the right to delete any comment without notice explanations.</p>
			<form method="POST" action="{{ route('shareafoodcomments.store') }}">
				<div class="f-row">
					<div class="third">
						<input type="text"  name="name" id="name" placeholder="Your name" />
						<span class="req">*</span>
					</div>

					<div class="form-group">
						<input type="hidden" class="form-control" value="{{$shareafood->id}}" name="shareafood_id" id="{{$shareafood->id}}">

					</div>

				</div>
				<div class="f-row">
					<textarea name="comment" id="comment"></textarea>
				</div>

				<div class="f-row">
					<div class="third bwrap" style="padding-bottom: 40px">
						<input type="submit" value="Submit comment" />
					</div>
				</div>


				{{ csrf_field() }}
			</form>
		</div>
	</div>




		{{--<div class="row">
			<form method="POST" class="col-md-8 col-md-offset-2" style="margin-top: 50px;" action="{{ route('shareafoodcomments.store') }}">
				--}}{{--<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" name="Name" autocomplete="off" id="Name" placeholder="Name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="email" class="form-control" name="email" autocomplete="off" id="email" placeholder="E-mail">
						</div>
					</div>
				</div>--}}{{--
				<div class="row">
				<div class="col-md-6">
					<label name="comment">User Name:</label>
					<div class="form-group">
						<input type="text" class="form-control"  name="name" id="name">
					</div>
				</div>
				</div>
				--}}{{--<div class="col-md-6">--}}{{--
					<div class="form-group">
						<input type="hidden" class="form-control" value="{{$shareafood->id}}" name="shareafood_id" id="{{$shareafood->id}}">
					</div>
			--}}{{--	</div>--}}{{--

				<div class="row">
					<div class="col-md-12">
						<label name="comment">comment:</label>
						<div class="form-group">
							<textarea class="form-control textarea" rows="3" name="comment" id="comment" placeholder="comment"></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn main-btn pull-right">comment</button>
					</div>
				</div>
				{{ csrf_field() }}
			</form>

			--}}{{--<dl class="dl-horizontal">
				<label>Url:</label>
				<p><a href="{{ route('blog.single', $post->slug) }}">{{ route('blog.single', $post->slug) }}</a></p>
			</dl>--}}{{--
		--}}{{--<div id="comment-form" class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
			{{ Form::open(['route' => ['reviewcomments.store'/*, $reviews->id*/], 'method' => 'POST']) }}

				<div class="row">

					<div class="col-md-6">
						{{ Form::label('review_id', "review_id:") }}
						{{ Form::text('review_id', null, ['class' => 'form-control']) }}
					</div>
				--}}{{----}}{{--
					<div class="col-md-6">
						{{ Form::label('email', 'Email:') }}
						{{ Form::text('email', null, ['class' => 'form-control']) }}
					</div>--}}{{----}}{{--

					<div class="col-md-12">
						{{ Form::label('comment', "Comment:") }}
						{{ Form::textarea('comment', null, ['class' => 'form-control', 'rows' => '5']) }}

						{{ Form::submit('Add Comment', ['class' => 'btn btn-success btn-block', 'style' => 'margin-top:15px;']) }}
					</div>
				</div>

			{{ Form::close() }}
		</div>--}}{{--
	</div--}}>

@endsection
