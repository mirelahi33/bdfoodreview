
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BdFoodReview</title>
    <link href="{{ asset('css/bootstrap.min.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/Pretty-Footer.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/styles.css')}}" media="all" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('css/signup-popup.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/untitled.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('fonts/font-awesome.min.css')}}" media="all" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bree+Serif">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Permanent+Marker">

    <style>
        #footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px; /* Height of the footer */
            background: #6cf;
        }
    </style>

