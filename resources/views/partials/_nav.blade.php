<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header"><a class="navbar-brand navbar-link" href="/home">BdFoodReview </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span
                        class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav navbar-right"></ul>
            <ul class="nav navbar-nav">

                <li role="presentation"><a href="/reviews">Reviews </a></li>
                <li role="presentation"><a href="/offer">Offers </a></li>
                <li role="presentation"><a href="/shareafood">Share A Food</a></li>
                <li role="presentation"><a href="/foodgraphy">FoodGraphy </a></li>
                <li role="presentation"><a href="/map">Search in Map </a></li>
                <li role="presentation"><a href="/contact">Contact US </a></li>

                {{--    @can('isSuperAdmin')--}}
                <li role="presentation"><a href="/admin_create">create </a></li>

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest


                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>

                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                           aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                    @endguest

            </ul>

        </div>
    </div>
</nav>