@extends('main')

@section('content')

    <div class="row">
        <div class="col-md-10">

            <h1>All Post</h1>
            <hr>
        </div>
        <div class="col-md-2">

            <a href="{{ route('offer.create') }}" class="button large">create offers</a>

        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table" id="offers">

                    <thead>
                    <th>#</th>
                    <th>restaurant name</th>
                    <th>item name</th>
                    <th>Area name</th>
                    <th>Offer Name</th>
                    <th>offer image</th>
                    <th>Offer details</th>
                    <th>Offer price</th>
                    <th>Offer duration from</th>
                    <th>Offer duration to</th>

                    </thead>
                    <tbody>
                    @foreach($offers as $offer)
                        <tr>
                            <th>{{$offer->id}}</th>
                            <td>{{$offer->restaurant->restaurant_name}}</td>
                            <td>{{$offer->item->item_name}}</td>
                            <td>{{$offer->area->area_name}}</td>
                            <td>{{$offer->offer_name}}</td>

                            @if ($offer->offer_image==null)  <td></td>
                            @else  <td><img src="{{asset('/images/'.$offer->offer_image)}}" alt="offer_image" height="200" width="200"></td>@endif
                            <td>{{$offer->offer_details}}</td>

                            <td>{{$offer->price}}</td>
                            <td>{{$offer->offer_duration_from}}</td>
                            <td>{{$offer->offer_duration_to}}</td>


                            {{--<td>{{str_limit($restaurant->address, 20)}}</td>
                            <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
                            <td>  <div class="row">
                                    <div class="col-sm-4">
                                        <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
                                    </div>
                                    <div class="col-sm-4">
                                        --}}{{----}}{{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}{{----}}{{--
                                        <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                                            <input type="submit" value="Delete" class="btn btn-danger btn-block">
                                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                                            {{ method_field('DELETE') }}
                                        </form>﻿
                                    </div>
                                </div>﻿</td>--}}
                        </tr>
                    @endforeach

                    </tbody>
                </table>


            </div>

        </div>

    </div>﻿

@endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#offers').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection