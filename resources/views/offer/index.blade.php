@extends('main')

@section('content')

    <div class="wrap clearfix">
        <!--breadcrumbs-->
        <nav class="breadcrumbs">
            <ul>
                <li><a href="/home" title="Home">Home</a></li>
                <li>Offers</li>
            </ul>
        </nav>
        <!--//breadcrumbs-->

        <!--row-->
        <div class="row">
            <header class="s-title">
                <h1>Offers</h1>
            </header>

            <!--content-->
            <section class="content three-fourth">
                <!--entries-->
                <div class="entries row">
                    <!--item-->
                    @foreach($offers as $offer)
                        <div class="entry one-third">
                            <figure>
                                @if ($offer->offer_image==null) <img src="{{asset('images/no.jpg')}}"height="200" width="270" alt="offer image">
                                @else  <img src="{{asset('images/'.$offer->offer_image)}}"height="200" width="270" alt="offer image">
                            @endif
                                <figcaption><a href="{{ route('offer.single', $offer->slug) }}"><i class="icon icon-themeenergy_eye2"></i> <span>View recipe</span></a></figcaption>
                            </figure>
                            <div class="container">
                                <h2>{{ substr(strip_tags($offer->offer_name), 0, 30) }}{{ strlen(strip_tags($offer->offer_name)) > 30 ? '...' : "" }}</h2>

                                {{--<h2>{{ $offer->offer_name }}</h2>--}}
                                <p>Published: {{ date('M j, Y', strtotime($offer->created_at)) }}</p>

                                <p>{{ substr(strip_tags($offer->offer_details), 0, 30) }}{{ strlen(strip_tags($offer->offer_details)) > 30 ? '...' : "" }}</p>

                                <a href="{{ route('offer.single', $offer->slug) }}" class="button">Read More</a>

                                {{-- <h2><a href="recipe.html">Thai fried rice with fruit and vegetables</a></h2>--}}
                                {{--<div class="actions">
                                    <div>
                                        <div class="difficulty"><i class="ico i-medium"></i><a href="#">medium</a></div>
                                        <div class="likes"><i class="fa fa-heart"></i><a href="#">10</a></div>
                                        <div class="comments"><i class="fa fa-comment"></i><a href="recipe.html#comments">27</a></div>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                @endforeach
                <!--item-->



                </div>
                <!--//entries-->
            </section>
            <!--//content-->

            <!--right sidebar-->
            <aside class="sidebar one-fourth">
                <div class="widget">
                    <ul class="categories right">
                        <li class="active"><a href="#">All Catagories</a></li>
                        <li><a href="#">Apetizers</a></li>
                        <li><a href="#">Soups</a></li>
                        <li><a href="#">Snacks</a></li>
                        <li><a href="#">Fish</a></li>
                        <li><a href="#">Steaks</a></li>
                        <li><a href="#">Pizzas</a></li>
                        <li><a href="#">Asian</a></li>
                        <li><a href="#">Mexican</a></li>
                        <li><a href="#">Vegetarian</a></li>
                        <li><a href="#">Salads</a></li>
                        <li><a href="#">Cocktails</a></li>
                        <li><a href="#">Desserts</a></li>

                    </ul>
                </div>

            </aside>
            <!--//right sidebar-->
        </div>
        <!--//row-->
    </div>

@endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#offers').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection



