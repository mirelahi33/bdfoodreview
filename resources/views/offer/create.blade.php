@extends('main')

@section('content')

    <div class="row">

        <section class="content center full-width">
            <div class="modal container">
                <h1 class="center">Create an Offer</h1>

                <form  method="POST" enctype="multipart/form-data" action="{{ route('offer.store') }}" {{--name="contactform" id="contactform"--}}>

                    <div id="message" class="alert alert-danger"></div>
                    <div class="f-row">

                        <label name="restaurant_name">Restaurant name:</label>
                        <input name="restaurant_name" id="restaurant_name" value="{{ \Auth::user()->restaurant->restaurant_name }}" class="form-control"readonly>
                    </div>


                    <div class="f-row">
                        <label name="item_name">Item name:</label>
                        <select name="item_name" id="item_name" class="form-control" required="">
                            <option value=""> -- Select One --</option>
                            @foreach ($items as $item_name)
                                <option value="{{ $item_name->id }}">{{ $item_name->item_name }}</option>
                            @endforeach
                        </select>

                    <div class="f-row">
                        <label name="area_name">Area name:</label>

                        <input type="text" name="area_name" id="area_name" value="{{ \Auth::user()->restaurant->area->area_name }}" class="form-control"readonly>
                    </div>

                    <div class="f-row">
                        <label name="offer_name">Offer Name:</label>
                        <input type="text" type="text" id="offer_name" name="offer_name" class="form-control" required="">
                    </div>

                    <div class="f-row">
                        <label name="offer_details">Offer details:</label>
                        <input type="text" id="offer_details" name="offer_details"  class="form-control" required="">
                    </div>
                    <div class="f-row">
                        <label name="offer_price">Offer price:</label>
                        <input type="text" id="offer_price" name="offer_price" class="form-control" required="">
                    </div>
                    <div class="f-row">
                        <label name="offer_duration">Offer duration:</label>
                        <div class="f-row">

                            <div >
                                <label name="offer_duration_from">From: </label>
                                <input id="offer_duration_from"  name="offer_duration_from" type="date" class="form-control">
                            </div>
                            <div >
                                <label name="offer_duration_to">To: </label>
                                <input id="offer_duration_to"  name="offer_duration_to" type="date" class="form-control">
                            </div>
                        </div>
                    </div>
                        <div class="f-row">
                            <label class="col-md-3 control-label" >image:</label>
                            <div class="col-md-8">
                                <input  type="file" id="featured_img" placeholder="offer_img" name="featured_img" class=""/>
                                <span class="required" id='spnFileError'></span>
                            </div>
                        </div>


                    <div class="f-row bwrap">
                        <input type="submit" value="Create an Offer" />
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
            </div>
        </section>
    </div>


    @endsection


{{--  <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>create an offer</h1>
            <hr>
            <form method="POST" action="{{ route('offer.store') }}">
                <div class="form-group">
                    <label name="restaurant_name">Restaurant name:</label>
                    <input name="restaurant_name" id="restaurant_name" value="{{ \Auth::user()->restaurant->restaurant_name }}" class="form-control"readonly>
                </div>
                <div class="form-group">
                    <label name="item_name">Item name:</label>
                    <select name="item_name" id="item_name" class="form-control">
                        <option value=""> -- Select One --</option>
                        @foreach ($items as $item_name)
                            <option value="{{ $item_name->id }}">{{ $item_name->item_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label name="area_name">Area name:</label>

                    <input name="area_name" id="area_name" value="{{ \Auth::user()->restaurant->area->area_name }}" class="form-control"readonly>


                </div>
                <div class="form-group">
                    <label name="offer_name">Offer Name:</label>
                    <input id="offer_name" name="offer_name" class="form-control">
                </div>
                <div class="form-group">
                    <label name="offer_details">Offer details:</label>
                    <input id="offer_details" name="offer_details" class="form-control">
                </div>
                <div class="form-group">
                    <label name="offer_price">Offer price:</label>
                    <input id="offer_price" name="offer_price" class="form-control">
                </div>
            <div class="form-group">
                <label name="offer_duration">Offer duration:</label>
                <div class="row">

                <div class="col-md-6 ">
                <input id="offer_duration_from" name="offer_duration_from" type="date" class="form-control">
                </div>
                    <div class="col-md-6 ">
                <input id="offer_duration_to" name="offer_duration_to" type="date" class="form-control">
                    </div>
                </div>
            </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted"></small>
                </div>
                <input type="submit" value="give review" class="btn btn-success btn-lg btn-block">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </div>﻿--}}