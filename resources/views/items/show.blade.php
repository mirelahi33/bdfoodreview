@extends('main')

@section('content')

    <div class="row">
        <div class="col-md-8 ">
            <h1>{{$item->item_name}}</h1>
            <p class="form-control">{{$item->price}} </p>
        </div>
    </div>﻿


        <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('item.edit', $item->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
             {{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}
                <form method="POST" action="{{ route('item.destroy', $item->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿







    @endsection