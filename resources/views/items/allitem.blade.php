@extends('main')

@section('content')

    <div class="row">
       <div class="col-md-10">

           <h1>All Items</h1>

       </div>


<div class="row">
    <div class="col-md-12">
        <table class="table" id="items">

            <thead>
            <th>Items name</th>


            <th>item image</th>

            <th>Restaurant name</th>
            <th>Price</th>


            </thead>
<tbody>
  @foreach($items as $item)
<tr>
    <td>{{$item->item_name}}</td>
@if ($item->item_image==null)  <td></td>
  @else  <td><img src="{{asset('/images/'.$item->item_image)}}" alt="item_image" height="200" width="200"></td>@endif

    <td>{{$item->restaurant->restaurant_name}}</td>

    <td>{{$item->price}}</td>

  {{--  <td>{{$item->owners_recommendation}}</td>--}}



    {{--<td>{{str_limit($restaurant->address, 20)}}</td>
    <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
    <td>  <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
                --}}{{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}{{--
                <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿</td>--}}
</tr>
@endforeach

</tbody>
        </table>


    </div>

</div>

    </div>﻿

    @endsection

@section('page_js')
    <script>
        $(function () {
            var table = $('#items').DataTable(
                {
                    "pageLength": 50
                }
            )
        })

    </script>
@endsection