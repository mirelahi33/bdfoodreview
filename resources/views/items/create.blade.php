@extends('main')

@section('content')
    <div class="row">
        <!--content-->
        <section class="content center full-width">
            <div class="modal container">
                <form  method="POST" enctype="multipart/form-data" action="{{ route('item.store') }}" {{--name="contactform" id="contactform"--}}>
                    <h1 class="center">Create an Item</h1>
                    <div id="message" class="alert alert-danger"></div>
                    <div class="f-row">
                        <label name="item_name">Item Name:</label>
                        <input type="text" id="item_name" name="item_name" class="form-control" required="">
                    </div>

                    <div class="f-row">
                        <label name="price">Price:</label>
                        <input type="text" id="price" name="price" class="form-control" required="">
                    </div>


                    <div class="f-row">
                        <label class="col-md-3 control-label" >image:</label>
                        <div class="col-md-8">
                            <input  type="file" id="featured_img" placeholder="featured_img" name="featured_img" class=""/>
                            <span class="required" id='spnFileError'></span>
                        </div>
                    </div>



                    <div class="f-row bwrap">
                        <input type="submit" value="Insert Item" />
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
            </div>
        </section>
        <!--//content-->
    </div>



    @endsection

{{--
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Item input</h1>
        <hr>
        <form method="POST" action="{{ route('item.store') }}">
            <div class="form-group">
                <label name="item_name">Item Name:</label>
                <input id="item_name" name="item_name" class="form-control">
            </div>

            <div class="form-group">
                <label name="price">Price:</label>
                <input id="price" name="price" class="form-control">
            </div>


            --}}
{{--   <div class="form-group">
                   <label name="active">Owners recomendation:</label>
                   <select name="active">
                       <option value="yes" id="active" class="form-control">yes</option>
                       <option value="no" id="active" class="form-control">no</option>
                   </select>
               </div>--}}{{--




            --}}
{{--  <div class="form-group">
                  <label name="restaurant_name">Restaurant name:</label>
                  <select name="restaurant_name" id="restaurant_name" class="form-control">
                      <option value=""> -- Select One --</option>
                      @foreach ($x as $restaurant_name)
                          <option value="{{ $restaurant_name->id }}">{{ $restaurant_name->restaurant_name }}</option>
                      @endforeach
                  </select>
              </div>--}}{{--


            <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                <small id="fileHelp" class="form-text text-muted"></small>
            </div>



            <input type="submit" value="give review" class="btn btn-success btn-lg btn-block">
            <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
</div>﻿--}}
