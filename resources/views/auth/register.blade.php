@extends('main')

@section('content')

    <div class="wrap clearfix">
        <!--row-->
        <div class="row">
            <!--content-->
            <section class="content center full-width">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('register') }}">
                    {{ csrf_field() }}

                <div class="modal container">
                    <h3>Register</h3>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="f-row">
                        {{--<input type="text" placeholder="Your name" />--}}
                        <input id="name"  placeholder="Your name"  type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif

                    </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="f-row">
                        {{--<input type="email" placeholder="Your email" />--}}
                        <input id="email" placeholder="Your email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    </div>


                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="f-row">
                        {{--<input type="password" placeholder="Your password" />--}}
                        <input id="password" placeholder="Your password"  type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    </div>


                    <div class="f-row">
                        {{--<input type="password" placeholder="Retype password" />--}}
                        <input id="password-confirm" placeholder="Retype password"  type="password" class="form-control" name="password_confirmation" required>

                    </div>
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        <label for="gender" class="col-md-4 control-label">Role</label>

                        <div class="col-md-6">
                            <select class="form-control" name="gender">
                                <option value="user">user</option>
                                <option value="restaurantmoderator">Restaurant Admin</option>
                            </select>

                            @if ($errors->has('gender'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="f-row">
                        <label class="col-md-3 control-label" >image:</label>
                        <div class="col-md-8">
                            <input  type="file" id="featured_img" placeholder="shareafood_img" name="featured_img" class=""/>
                            <span class="required" id='spnFileError'></span>
                        </div>
                    </div>




                    <div class="f-row bwrap">
                        {{--<input type="submit" value="register" />--}}
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                    <p>Already have an account yet? <a href="/login">Log in.</a></p>
                </div>
                </form>
            </section>
            <!--//content-->
        </div>
        <!--//row-->
    </div>


{{--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
