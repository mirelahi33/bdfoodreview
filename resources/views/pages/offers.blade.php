@extends('main')
@section('content')

    <form>
        <p id="offerspara">Find Offers</p>
        <select class="form-control">
            <optgroup label="Search By">
                <option value="12" selected="">Location</option>
                <option value="13">Price</option>
            </optgroup>
        </select>



        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Restaurant</th>
                    <th>Offer name</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Pasta State</td>
                    <td>Buy one get one</td>
                </tr>
                <tr>
                    <td>Cafe Entro</td>

                    <td>Happy hour</td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    @endsection