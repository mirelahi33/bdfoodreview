@extends('main')
@section('content')

    <div class="container">
        <div class="btn-group-vertical btn-block" id="fix">

            <a href="/restaurant" class="btn btn-default">Add restaurant</a>
            <a href="/item"  class="btn btn-default">Add item</a>
            <a href="/area"  class="btn btn-default">Add Area</a>
            <a href="/offer"  class="btn btn-default">Add Offer </a>
            <a href="/shareafood"  class="btn btn-default">Share a food</a>
            <a href="/reviews"  class="btn btn-default">Moderate Review</a>
            <a href="/users"  class="btn btn-default">Users</a>


        </div>
    </div>




    @endsection