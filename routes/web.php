<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('pages.home');
});*/

Route::get('/vue', function () {
    return view('welcome');
});

Route::get('/','HomeController@index');
Route::get('/home','HomeController@index');
Route::post('/gg', 'ItemController@gg');

Route::get('/contact', 'HomeController@contact');
Route::get('/about', 'HomeController@about');

//Route::get('/review','PostsController@show_review');
//Route::get('/write_a_review','PostsController@write_a_review')->middleware('auth');
/*Route::get('/offers','PostsController@offers');*/
/*Route::get('/share_a_food','ShareafoodController@share_a_food')->middleware('auth');*/
Route::get('/foodgraphy','PostsController@foodgraphy');
Route::get('/map','MapController@index');
Route::get('/item/all','ItemController@allitem');

Route::get('/owners/all','HomeController@owners');


Route::get('/restaurant/all','RestaurantController@allrestaurant');


Route::get('/offer/search','OfferController@offer_search')->name('offer.search');
Route::get('/shareafood/search','ShareafoodController@shareafood_search')->name('shareafood.search');
Route::get('/reviews/search','ReviewsController@reviews_search')->name('reviews.search');






Route::get('reviews/tag/{slug}', 'ReviewsController@getSingle')->name('reviews.single')/*->where('slug', '[\w\d\-\_]+')*/;
Route::get('offer/tag/{slug}', 'OfferController@getSingle')->name('offer.single')/*->where('slug', '[\w\d\-\_]+')*/;
Route::get('shareafood/tag/{slug}', 'ShareafoodController@getSingle')->name('shareafood.single')/*->where('slug', '[\w\d\-\_]+')*/;
Route::get('restaurant/tag/{slug}', 'RestaurantController@getSingle')->name('restaurant.single');
Route::get('item/tag/{slug}', 'ItemController@getSingle')->name('item.single');


/*
Route::group(['middleware' => ['auth']], function () {



});*/

Route::resource('post','PostsController');
Route::resource('restaurant','RestaurantController');
Route::resource('item','ItemController');
Route::resource('shareafood','ShareafoodController');
Route::resource('reviews','ReviewsController');
Route::resource('area','AreaController');
Route::resource('offer','OfferController');
Route::resource('users','UserController');

Route::resource('reviewcomments','ReviewcommentController');
Route::resource('offercomments','OffercommentController');
Route::resource('shareafoodcomments','ShareafoodcommentController');


//Route::post('reviews/{slug}', ['as' => 'reviews.single', 'uses' => 'ReviewsController@store'])->where('slug', '[\w\d\-\_]+');




/*
Route::get('/admin_review','AdminController@show_review');
Route::get('/admin_write_a_review','AdminController@write_a_review');
Route::get('/admin_offers','AdminController@offers');
Route::get('/admin_share_a_food','AdminController@share_a_food');
Route::get('/admin_foodgraphy','AdminController@foodgraphy');
Route::get('/admin_home','AdminController@index');*/
Route::get('/admin_create','AdminController@create_pages');




Auth::routes();

/*Route::prefix('admin')->group(function (){
    Route::get('/login','/Auth/AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login','/Auth/AdminLoginController@login')->name('admin.login.submit');
    Route::get('/','AdminController@index')->name('admin.dashboard');

});*/
/*Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');


});*/
Route::post('/get_restaurant_info', 'RestaurantController@get_restaurants')->name('get_restaurants');
Route::post('/get_all_restaurant', 'RestaurantController@get_all_restaurant')->name('get_all_restaurant');
Route::post('/all_restaurants', 'RestaurantController@all_restaurant')->name('all_restaurant');
Route::post('/get_items', 'ItemController@get_items')->name('get_items');
Route::post('/get_area', 'AreaController@get_areas')->name('get_areas');
Route::post('/all_area', 'AreaController@all_areas')->name('all_areas');
Route::post('/get_restaurant_infos', 'RestaurantController@get_restaurant_info')->name('get_restaurant_infos');
//Route::get('/home', 'HomeController@index')->name('home');



/*Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');*/