-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2018 at 08:11 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `area_name` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `area_name`, `created_at`, `updated_at`) VALUES
(1, 'Khilgaon', '2018-05-01 18:18:14', '2018-05-01 18:18:14'),
(7, 'Baily Road', '2018-05-11 04:06:07', '2018-05-11 04:06:07'),
(8, 'Dhanmondi', '2018-05-11 04:06:23', '2018-05-11 04:06:23'),
(9, 'Banani', '2018-05-11 04:06:46', '2018-05-11 04:06:46'),
(10, 'Shyamoli, Mirpur', '2018-05-11 04:06:59', '2018-05-11 04:06:59'),
(11, 'Gulshan', '2018-05-11 04:07:30', '2018-05-11 04:07:30'),
(12, 'Motijheel', '2018-05-11 04:07:43', '2018-05-11 04:07:43');

-- --------------------------------------------------------

--
-- Table structure for table `area_restaurants`
--

CREATE TABLE `area_restaurants` (
  `id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `area_restaurants`
--

INSERT INTO `area_restaurants` (`id`, `area_id`, `restaurant_id`) VALUES
(1, 1, 1),
(9, 1, 19),
(10, 1, 20),
(11, 7, 21),
(12, 8, 22),
(13, 7, 23),
(14, 7, 24),
(15, 1, 25),
(16, 8, 26),
(17, 12, 27);

-- --------------------------------------------------------

--
-- Table structure for table `foodgraphies`
--

CREATE TABLE `foodgraphies` (
  `id` int(10) UNSIGNED NOT NULL,
  `foodgraphy_caption` varchar(119) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foodgraphy_image` blob,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `restaurant_id` int(10) UNSIGNED DEFAULT NULL,
  `item_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owners_recommendation` tinyint(1) DEFAULT '0',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `price`, `restaurant_id`, `item_image`, `owners_recommendation`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'jazz Lovers', 400, 1, 'jazz.jpg', 0, 'jazz-Lovers', '2018-05-01 18:18:54', '2018-05-12 11:12:01'),
(27, 'Shahi Kacchi Plater', 250, 19, '1526029154.jpg', 0, 'Shahi', '2018-05-11 02:59:14', '2018-05-11 03:23:24'),
(28, 'Plain Pulao & Beef Rezala', 230, 19, 'rez.jpg', 0, 'Plain', '2018-05-11 03:02:05', '2018-05-11 03:23:24'),
(29, 'Jorda', 60, 19, '1526029724.jpg', 1, 'Jorda', '2018-05-11 03:08:45', '2018-05-11 03:23:24'),
(30, 'Plain Pulao, Beef Rezala, Chicken Tanduri, Jorda, Coke, Borhani', 520, 19, '1526030169.jpg', 0, 'Plain-Pulao', '2018-05-11 03:16:09', '2018-05-11 03:23:24'),
(31, 'Pasta Appeliano', 285, 20, '1526032384.jpg', 0, 'Pasta-Appeliano', '2018-05-11 03:53:04', '2018-05-11 03:57:12'),
(32, 'Pizza Appeliano', 510, 20, '1526032502.jpg', 1, 'Pizza-Appeliano', '2018-05-11 03:55:03', '2018-05-11 03:55:17'),
(33, 'Virgin Mojito and Citrus Spider', 250, 21, '1526033409.jpg', 0, 'Virgin', '2018-05-11 04:10:09', '2018-05-11 04:10:09'),
(34, 'Brownie with Ice-cream', 150, 21, '1526033441.jpg', 1, 'Brownie-with Ice-cream', '2018-05-11 04:10:41', '2018-05-11 04:10:49'),
(35, 'Fried Rice', 150, 1, '1526036435.jpg', 1, 'Fried-Rice', '2018-05-11 05:00:35', '2018-05-12 11:12:01'),
(36, 'Pasta State Special', 350, 22, '1526100989.jpg', 0, 'pasta-state-special', '2018-05-11 22:56:29', '2018-05-11 22:56:29'),
(37, 'Beef Bacon Burger', 250, 23, NULL, 1, 'beef-bacon-burger', '2018-05-12 11:35:29', '2018-05-12 11:35:38'),
(38, 'Chicken Cheese Burger', 160, 23, NULL, 0, 'chicken-cheese-burger', '2018-05-12 11:53:54', '2018-05-12 11:53:54'),
(39, 'Pizzgar', 195, 24, '1526148628.jpg', 0, 'pizzgar', '2018-05-12 12:10:28', '2018-05-12 12:10:28'),
(40, 'Beef Steak', 650, 24, NULL, 1, 'beef-steak', '2018-05-12 12:15:05', '2018-05-12 12:15:11'),
(41, 'Signature Deep Dish Pizza', 990, 25, '1526161998.jpg', 1, 'signature-deep-dish-pizza', '2018-05-12 15:53:19', '2018-05-12 15:53:24');

-- --------------------------------------------------------

--
-- Table structure for table `item_restaurants`
--

CREATE TABLE `item_restaurants` (
  `id` int(255) NOT NULL,
  `restaurant_id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_restaurants`
--

INSERT INTO `item_restaurants` (`id`, `restaurant_id`, `item_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_14_171015_create_posts_table', 1),
(4, '2017_11_15_095441_create_restaurants_table', 1),
(5, '2017_11_23_123848_create_admins_table', 1),
(6, '2018_01_22_142334_create_shareafoods_table', 1),
(7, '2018_04_02_102429_create_reviews_table', 1),
(8, '2018_04_04_171753_create_items_table', 1),
(9, '2018_04_06_161048_create_areas_table', 1),
(10, '2018_04_06_184032_create_offers_table', 1),
(11, '2018_04_06_191309_create_foodgraphies_table', 1),
(12, '2018_04_22_160749_add_offer_id_shareafoods', 1),
(13, '2018_04_22_202908_offer_shareafood', 2),
(14, '2018_05_04_015153_area_restaurant', 3),
(15, '2018_05_04_235848_area_restaurants', 4),
(16, '2018_05_06_052754_create_reviewcomments_table', 5),
(17, '2018_05_06_165442_reviewcomments', 6),
(18, '2018_05_08_054317_create_offercomments_table', 7),
(19, '2018_05_08_055418_create_shareafoodcomments_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `offercomments`
--

CREATE TABLE `offercomments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `offer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offercomments`
--

INSERT INTO `offercomments` (`id`, `name`, `comment`, `approved`, `user_id`, `offer_id`, `created_at`, `updated_at`) VALUES
(1, 'hridima', 'sdadasdasd', 1, 1, 1, '2018-05-08 00:22:52', '2018-05-08 00:22:52'),
(2, 'Ayon Elahi', 'nice offer', 1, 1, 7, '2018-05-12 04:29:43', '2018-05-12 04:29:43'),
(3, 'Dewan Shahrin', 'Wow!!!', 1, 27, 8, '2018-05-12 11:57:28', '2018-05-12 11:57:28'),
(4, 'Ayon Elahi', 'nICE', 1, 27, 8, '2018-05-12 11:58:04', '2018-05-12 11:58:04'),
(5, 'user', 'i think we should try it.', 1, 42, 9, '2018-05-12 22:26:39', '2018-05-12 22:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_name` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_details` varchar(119) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_duration_from` date DEFAULT NULL,
  `offer_duration_to` date DEFAULT NULL,
  `offer_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restaurant_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `area_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `offer_name`, `offer_details`, `price`, `offer_duration_from`, `offer_duration_to`, `offer_image`, `restaurant_id`, `item_id`, `area_id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Buy 1 Get 1', 'Buy 1 Get 1 offer on Jazz Lovers', '360', '2018-05-16', '2018-05-25', 'jazz.jpg', 1, 1, 1, 'buyoneget1', '2018-05-03 19:20:02', '2018-05-03 19:20:02'),
(7, 'Pulao Platter', 'There will be Plain Pulao, Beef Rezala, Chicken Tanduri (Quarter), Jorda, Borhani and a Coke', '480', '2018-05-15', '2018-06-15', '1526030423.jpg', 19, 30, 1, 'pulao-platter', '2018-05-11 03:20:24', '2018-05-11 03:20:24'),
(8, 'Double Dhamaka', '4 chicken cheese burger at the price of two', '450', '2018-05-10', '2018-05-16', NULL, 23, 38, 7, 'double-dhamaka', '2018-05-12 11:55:31', '2018-05-12 11:55:31'),
(9, 'Buy 1 get 1', 'Buy 1 get 1 free on steak', '700', '2018-05-01', '2018-05-17', '1526149046.jpg', 24, 40, 7, 'buy-1-get-1', '2018-05-12 12:17:26', '2018-05-12 12:17:26');

-- --------------------------------------------------------

--
-- Table structure for table `offer_restaurants`
--

CREATE TABLE `offer_restaurants` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(100) NOT NULL,
  `offer_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offer_restaurants`
--

INSERT INTO `offer_restaurants` (`id`, `restaurant_id`, `offer_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `offer_shareafood`
--

CREATE TABLE `offer_shareafood` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_id` int(10) UNSIGNED NOT NULL,
  `shareafood_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(10) UNSIGNED NOT NULL,
  `restaurant_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restaurant_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `restaurant_logo`, `restaurant_name`, `address`, `slug`, `area_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'tune.png', 'Tune & Bites Music Cafe', 'Taltola, Khilgaon', 'bfc', 1, 1, '2018-05-01 18:18:30', '2018-05-01 18:18:30'),
(19, '1526029036.jpg', 'Tradition BD', 'Khilgaon, Taltola', 'tradition-bd', 1, 19, '2018-05-11 02:57:16', '2018-05-11 02:57:16'),
(20, '1526032295.jpg', 'Cafe Appeliano', 'Taltola, Khilgaon', 'cafe-appeliano', 1, 21, '2018-05-11 03:51:35', '2018-05-11 03:51:35'),
(21, '1526033360.jpeg', 'BruTown Cafe', '1/A, New Baily Road, Dhaka', 'brutown-cafe', 7, 22, '2018-05-11 04:09:20', '2018-05-11 04:09:20'),
(22, '1526100842.jpg', 'Pasta state', 'Almas, 8A Satmasjid Road, Dhanmondi 15, Dhaka 1207', 'pasta-state', 8, 26, '2018-05-11 22:54:04', '2018-05-11 22:54:04'),
(23, NULL, 'The Pabulum', 'Chamelibag, Opposite of Twin Tower', 'the-pabulum', 7, 28, '2018-05-12 11:34:04', '2018-05-12 11:34:04'),
(24, '1526148595.jpg', 'Coriander Restaurant', 'Level, house 2, Green cozy cottage,New, Baily Rd, Dhaka 100', 'coriander-restaurant', 7, 29, '2018-05-12 12:09:55', '2018-05-12 12:09:55'),
(25, '1526161766.png', 'The Dining Lounge', '373/B Level 3, Shotodol Rose Heights, Shahid Baki Road, Khilgaon Taltola Mor, Dhaka- 1219', 'the-dining-lounge', 1, 30, '2018-05-12 15:49:26', '2018-05-12 15:49:26');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_areas`
--

CREATE TABLE `restaurant_areas` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(100) NOT NULL,
  `area_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant_areas`
--

INSERT INTO `restaurant_areas` (`id`, `restaurant_id`, `area_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reviewcomments`
--

CREATE TABLE `reviewcomments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `reviews_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `reviewer_name` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `restaurant_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED DEFAULT NULL,
  `rating` double NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `review_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `reviewer_name`, `user_id`, `restaurant_id`, `item_id`, `area_id`, `rating`, `comment`, `review_image`, `slug`, `created_at`, `updated_at`) VALUES
(30, 'Fabaswir', 20, 19, 30, 1, 8, 'very nice taste.', '1526031515.jpeg', 'very-nice-taste', '2018-05-11 03:38:35', '2018-05-11 03:38:35'),
(32, 'Fabaswir', 20, 21, 34, 7, 10, 'It\'s amazing!!!!', NULL, 'it-s-amazing', '2018-05-11 04:18:07', '2018-05-11 04:18:07'),
(33, 'Swarnali Hridima', 18, 21, 34, 7, 10, 'Mouthwatering... The brownie was so soft and tastes really good.', '1526034020.jpg', 'mouthwatering-the-brownie-was-so-soft-and-tastes-really-good', '2018-05-11 04:20:20', '2018-05-11 04:20:20'),
(34, 'Swarnali Hridima', 18, 21, 33, 7, 7, 'Virgin Mojito was good but I liked the Citrus Spider more, its basically a combo of mint and orange juice but both of the items were too sour!', '1526034102.jpg', 'virgin-mojito-was-good-but-i-liked-the-citrus-spider-more-its-basically-a-combo-of-mint-and-orange-juice-but-both-of-the-items-were-too-sour', '2018-05-11 04:21:43', '2018-05-11 04:21:43'),
(35, 'Ayon', 24, 21, 33, 7, 6, 'Not so good in taste!', '1526034200.jpg', 'not-so-good-in-taste', '2018-05-11 04:23:20', '2018-05-11 04:23:20'),
(36, 'Naba', 25, 21, 33, 7, 8, 'Good and sour in taste , Citrus Spider is a beautiful combo of Orange and Mint Juice! Liked It', NULL, 'good-and-sour-in-taste-citrus-spider-is-a-beautiful-combo-of-orange-and-mint-juice-liked-it', '2018-05-11 04:25:38', '2018-05-11 04:25:38'),
(37, 'Dewan Shahrin', 27, 19, 30, 1, 9, 'very very tasty', NULL, 'very-very-tasty', '2018-05-12 11:25:05', '2018-05-12 11:25:05'),
(38, 'Dewan Shahrin', 27, 19, 30, 1, 8, 'Appealing', NULL, 'appealing', '2018-05-12 11:28:08', '2018-05-12 11:28:08'),
(39, 'afifa', 28, 19, 30, 1, 7, 'good', '1526146225.jpg', 'good', '2018-05-12 11:30:25', '2018-05-12 11:30:25'),
(40, 'khan', 29, 24, 39, 7, 4, 'Very bad taste..', '1526149196.jpg', 'very-bad-taste', '2018-05-12 12:19:56', '2018-05-12 12:19:56'),
(41, 'khan', 29, 19, 27, 1, 4, 'werwerwerwer', NULL, 'werwerwerwer', '2018-05-12 14:10:06', '2018-05-12 14:10:06'),
(44, 'Ayon Elahi', 1, 24, 40, 7, 9, 'Nyc', NULL, 'nyc', '2018-05-12 15:30:52', '2018-05-12 15:30:52'),
(45, 'Mir', 41, 23, 38, 7, 4, 'good', NULL, 'good-3', '2018-05-12 20:05:01', '2018-05-12 20:05:01'),
(46, 'Mir', 41, 23, 39, 1, 4, 'good', NULL, 'good-4', '2018-05-12 20:05:12', '2018-05-12 20:05:12'),
(47, 'Mir', 41, 21, 34, 7, 6, 'good', NULL, 'good-5', '2018-05-12 20:05:21', '2018-05-12 20:05:21');

-- --------------------------------------------------------

--
-- Table structure for table `shareafoodcomments`
--

CREATE TABLE `shareafoodcomments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `shareafood_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shareafoodcomments`
--

INSERT INTO `shareafoodcomments` (`id`, `name`, `comment`, `approved`, `user_id`, `shareafood_id`, `created_at`, `updated_at`) VALUES
(1, 'ayon', 'fsdf', 1, 1, 1, '2018-05-08 01:01:22', '2018-05-08 01:01:22'),
(2, 'Saief Sadat', 'I want to share this', 1, 27, 4, '2018-05-12 11:59:44', '2018-05-12 11:59:44'),
(3, 'hridima', 'lets try it', 1, 42, 6, '2018-05-12 22:31:12', '2018-05-12 22:31:12');

-- --------------------------------------------------------

--
-- Table structure for table `shareafoods`
--

CREATE TABLE `shareafoods` (
  `id` int(10) UNSIGNED NOT NULL,
  `shareafood_caption` varchar(119) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_person` double DEFAULT NULL,
  `estimate_date` date DEFAULT NULL,
  `estimate_time` varchar(119) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shareafood_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restaurant_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `area_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `offer_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shareafoods`
--

INSERT INTO `shareafoods` (`id`, `shareafood_caption`, `no_of_person`, `estimate_date`, `estimate_time`, `shareafood_image`, `restaurant_id`, `item_id`, `area_id`, `user_id`, `slug`, `created_at`, `updated_at`, `offer_id`) VALUES
(1, 'Jazz Lovers!!', NULL, '2018-05-08', '2:00 pm', 'jazz.jpg', 1, 1, 1, 1, 'jazz', '2018-05-04 18:54:23', '2018-05-04 18:54:23', 1),
(2, '01701307793', NULL, '2018-05-16', '8 pm', NULL, 23, 37, 7, 28, '01701307793', '2018-05-12 11:38:56', '2018-05-12 11:38:56', NULL),
(3, '01701307793', NULL, '2018-05-16', '8 pm', NULL, 23, 37, 7, 28, '01701307793-1', '2018-05-12 11:45:26', '2018-05-12 11:45:26', 7),
(4, '01701307793', NULL, '2018-05-16', '8 pm', NULL, 23, 37, 7, 28, '01701307793-2', '2018-05-12 11:47:14', '2018-05-12 11:47:14', 7),
(5, 'Mail me at fhgf@jj.com', NULL, '2018-05-18', '8 pm', '1526149142.jpg', 24, 40, 7, 29, 'mail-me-at-fhgfatjj-com', '2018-05-12 12:19:02', '2018-05-12 12:19:02', 9),
(6, 'contact me  01733331533', NULL, '2018-05-08', '9', NULL, 19, 28, 1, 42, 'contact-me-01733331533', '2018-05-12 22:30:20', '2018-05-12 22:30:20', 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `featured_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'user.png',
  `password` varchar(119) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `user_type`, `featured_img`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ayon Elahi', 'mirelahi33@gmail.com', 'superadmin', 'user.png', '$2y$10$HrU/tH44uSZKcxosqkuwxe7Afo1ECSC1L/fcircC4dlLkv5/GInAi', 'ohNlSEhl3my3ymWxZNcksKrIturhwnLR1pqXH3Tx4vTDGZPSDocJ2lLFvfcU', '2018-05-01 18:17:56', '2018-05-10 23:28:26'),
(18, 'Swarnali Hridima', 'swarnali.spring14@gmail.com', 'superadmin', 'user.png', '$2y$10$laJyPRNrbEDz5J/YCNQcvee2L56P5eNXqvFT9Ehi7QFbl67gwom4.', 'qdDGIk6Ctu7ElPEM8VydHUlpK7oRuB7WZfb1itkn2XoOV7KsJrAbFcwPb4cw', '2018-05-11 02:14:07', '2018-05-12 11:14:01'),
(19, 'Nafi', 'manfab123@gmail.com', 'restaurantmoderator', 'user.png', '$2y$10$d8kKwHxTZAxmXCTyHvRZIeSLzF1M8P1NbT8Zxy0IPU.JxWgfDsrUC', 'BUMzI4mpyBqtGAaHv4HERsTi3ysplANvbrNCjABHItnH8G3d1xR9Ml9BzpLU', '2018-05-11 02:54:05', '2018-05-11 02:54:05'),
(20, 'Fabaswir', 'fabu@gmail.com', 'user', 'user.png', '$2y$10$oL9ldDfEKTq0rmxZsvjhJOlEl86l5/HBPYWrI7Ho4M5/0DumpmbMy', 'YxVS9wCQolFMlRvLI9smeystqEWgATyk2hDOy0HinlfCU937Pkj8qz6MqQQU', '2018-05-11 03:36:59', '2018-05-11 03:36:59'),
(21, 'Smita Jahan', 'sj@gmail.com', 'restaurantmoderator', 'user.png', '$2y$10$EQpHKLHtKZaRbSr0OE2j5OiSriEuQDo6VIjyI6TgEiPwNOmvo0ar.', 'CrO7ayDFEORCEqUfOfrU14UlDX757VAWL0dAcgxwELQh8QYRNV4pBCKVtMha', '2018-05-11 03:49:30', '2018-05-11 03:49:30'),
(22, 'Saief Sadat', 'saief92@gmail.com', 'restaurantmoderator', 'user.png', '$2y$10$.Tmri57ppJ8BXxrCWDvpwem8uR8huzBaAnEPwdy.fKXK/T/tCmFBq', 'mfc5EmKeSaWsId5vGRErkbQeCMHedqnKeZmFhjQr536eOXPNjMqgOMNYxrKC', '2018-05-11 04:00:07', '2018-05-11 04:00:07'),
(23, 'Antar', 'antar@gmail.com', 'superadmin', 'user.png', '$2y$10$2vSTpIX9ZMHgSHIN5Iw5FuMEYp4ACoag9xVy.xEsJfBtG7i6jMOzW', 'lDYM9jQZ1yZnaUBXbEb2NOUD47TCA1nuLFo2igeyQVkt9e94ZjVbLBq0XIcL', '2018-05-11 04:05:14', '2018-05-11 04:05:14'),
(24, 'Ayon', 'ayon@gmail.com', 'user', 'user.png', '$2y$10$FJcE6LjSMk58gqFo3UI15uosXum/0fuvuKjyyl1fOp6IbIEZj1RZi', 'lpy67yNhZX9VlOgmtbfxuLG17rE8uZRUfXuIjrABeSju41BtHVWzfJymltXm', '2018-05-11 04:22:40', '2018-05-11 04:22:40'),
(25, 'Naba', 'naba@gmail.com', 'user', 'user.png', '$2y$10$ccsUTn6ds5vULlgzeuy/LOom.LZ0.MmGZxEJjhoE4hDc1n21eEX9O', 'fRQ1ADIMUa0CZYlu0NqeKKIvbLNNmOApolUq2SHHjKIW7aBc6iE3XpdHDcsD', '2018-05-11 04:24:49', '2018-05-11 04:24:49'),
(26, 'Mir', 'mir@gmail.com', 'restaurantmoderator', 'user.png', '$2y$10$IcNlP2pNWhVivlT06hwfxe4UlVqBfIMBaqLxWpoY4bhGkzJAwWnz6', 'XvetkYVEeR3aEMInmzwQx1TpMjJkbwW8knSc2fYSTg4PuqqVv6NLxYRhod9w', '2018-05-11 22:51:31', '2018-05-11 22:51:31'),
(27, 'Dewan Shahrin', 'shah_rin_pri_ya@yahoo.com', 'user', 'user.png', '$2y$10$Qa035610in3DLn497pUri.PcLTRLW0wL7pH37J3gWegoJp/JRh.oe', 'BiTirv3AyZjAYktSbcmZjBq9YCIXsxGgkVN1WvR7KfyUz9qdyBiAmMhOUGHA', '2018-05-12 11:23:08', '2018-05-12 11:23:08'),
(28, 'afifa', 'afifa@gmail.com', 'restaurantmoderator', 'user.png', '$2y$10$JurIJc7G8cCoxXSR.5ZuUOJLuLnwrjX9JEvbWLSJsIOUqtBnzAXku', 'JV8MYBN8VJr3P8pAgbBu7qFS53TRbmBm7vcUa4K4PZUR64kNL5quMfYGO3Yp', '2018-05-12 11:29:40', '2018-05-12 11:29:40'),
(29, 'khan', 'khan@gmail.com', 'user', 'user.png', '$2y$10$dfpq/HBQMw8hwPrjInxw3OGXW8d6QVpTvrBZJRBYIhm.rbGR9.eHu', 'G5bowhFMAhvPVRiUgYQNhuTgqWkiIWEJ9GmtHgGI3uDvWbIGr0WK22ANdGNL', '2018-05-12 12:02:38', '2018-05-12 12:02:38'),
(30, 'Rezoan Ahmed', 'abcd@gmail.com', 'restaurantmoderator', 'user.png', '$2y$10$LduVZIW0BEY3XYpOvDAecORjzN7.CSM9bU6.S/xKJEUZ6eRipv0gW', 'dGZSispemkAMfUf1wk5CADgvzInoxRtwsyjxdYiHnPEzgrcTaWQw5rCJ1hgP', '2018-05-12 15:37:40', '2018-05-12 15:37:40'),
(31, 'new', 'new@gmail.com', 'user', 'user.png', '$2y$10$ZohlT69NrAu..DBN0RReaebBUncVk57FXzcVfPmmFhec5gKQG9Bsu', '9qxl3qDsQPDG9QAWzMWVWXOkyNOZFE8rXKszZf7ubwJqOVviPw5Ld7BO2WeP', '2018-05-12 16:37:04', '2018-05-12 16:37:04'),
(41, 'Mir', 'mira@gmail.com', 'user', '5af79d30db0c0.jpg', '$2y$10$Kg86GPHMu2Tg9bBGgmg1O.DGuVAhFwK.GiriosFISx5KrFh3hKDyu', 'L7i4cUVhrlQAGcSgTIXCrOlQKeQzzW0c2oktb1VeJ892sEChjcRK1dYRtEgx', '2018-05-12 20:04:32', '2018-05-12 20:04:32'),
(42, 'user', 'user@gmail.com', 'user', '5af7ba2636543.jpg', '$2y$10$06aNr1XHGImBKZupOAF0ceFAEyaD0FPmGSL3HW9F9OzvBW3mTlN9.', 'AcoRfaQkgg2jssQ5sS7PDAyXD8VnyGyH6RS7lVIqJMkf3mCw8jTuabGJ05PC', '2018-05-12 22:08:06', '2018-05-12 22:08:06'),
(43, 'moderator', 'restaurant_moderator@gmail.com', 'restaurantmoderator', '5af7ba7cc7c77.jpg', '$2y$10$kazbS6LwN9SyESmbVGefjuK1M.jwoqkbpxenT/TEDQyHn5O/xdO9K', 'sanxrOKgH4S8BSwzOZ6K2TPdZGhsPsiFZEXsKcjLHmhg9kSajPifQekcGgUA', '2018-05-12 22:09:32', '2018-05-12 22:09:32'),
(44, 'admin', 'admin@gmail.com', 'superadmin', '5af7bad3bd856.jpg', '$2y$10$OdrAeCdi4yt/9aDw7B1IG.byVOYnjsUIAtujAYW8PwNhlU99sDVLa', 'YfA4rMbTW6i8j3SsAJ9cRyRieCsCJc3GFEDCXj1IBuoVs1xVMUCXbCgBojz8', '2018-05-12 22:10:59', '2018-05-12 22:48:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area_restaurants`
--
ALTER TABLE `area_restaurants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_restaurants_area_id_foreign` (`area_id`),
  ADD KEY `area_restaurants_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `foodgraphies`
--
ALTER TABLE `foodgraphies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_restaurants`
--
ALTER TABLE `item_restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offercomments`
--
ALTER TABLE `offercomments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offercomments_offer_id_foreign` (`offer_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_restaurants`
--
ALTER TABLE `offer_restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_shareafood`
--
ALTER TABLE `offer_shareafood`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_shareafood_offer_id_foreign` (`offer_id`),
  ADD KEY `offer_shareafood_shareafood_id_foreign` (`shareafood_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant_areas`
--
ALTER TABLE `restaurant_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviewcomments`
--
ALTER TABLE `reviewcomments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviewcomments_review_id_foreign` (`reviews_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shareafoodcomments`
--
ALTER TABLE `shareafoodcomments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shareafoodcomments_shareafood_id_foreign` (`shareafood_id`);

--
-- Indexes for table `shareafoods`
--
ALTER TABLE `shareafoods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shareafoods_user_id_foreign` (`user_id`),
  ADD KEY `shareafoods_offer_id_foreign` (`offer_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `area_restaurants`
--
ALTER TABLE `area_restaurants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `foodgraphies`
--
ALTER TABLE `foodgraphies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `item_restaurants`
--
ALTER TABLE `item_restaurants`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `offercomments`
--
ALTER TABLE `offercomments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `offer_restaurants`
--
ALTER TABLE `offer_restaurants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offer_shareafood`
--
ALTER TABLE `offer_shareafood`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `restaurant_areas`
--
ALTER TABLE `restaurant_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reviewcomments`
--
ALTER TABLE `reviewcomments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `shareafoodcomments`
--
ALTER TABLE `shareafoodcomments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shareafoods`
--
ALTER TABLE `shareafoods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `area_restaurants`
--
ALTER TABLE `area_restaurants`
  ADD CONSTRAINT `area_restaurants_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `area_restaurants_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`);

--
-- Constraints for table `offercomments`
--
ALTER TABLE `offercomments`
  ADD CONSTRAINT `offercomments_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offer_shareafood`
--
ALTER TABLE `offer_shareafood`
  ADD CONSTRAINT `offer_shareafood_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`),
  ADD CONSTRAINT `offer_shareafood_shareafood_id_foreign` FOREIGN KEY (`shareafood_id`) REFERENCES `shareafoods` (`id`);

--
-- Constraints for table `reviewcomments`
--
ALTER TABLE `reviewcomments`
  ADD CONSTRAINT `reviewcomments_review_id_foreign` FOREIGN KEY (`reviews_id`) REFERENCES `reviews` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shareafoodcomments`
--
ALTER TABLE `shareafoodcomments`
  ADD CONSTRAINT `shareafoodcomments_shareafood_id_foreign` FOREIGN KEY (`shareafood_id`) REFERENCES `shareafoods` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shareafoods`
--
ALTER TABLE `shareafoods`
  ADD CONSTRAINT `shareafoods_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`),
  ADD CONSTRAINT `shareafoods_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
